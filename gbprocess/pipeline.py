import logging
import multiprocessing
import sys
from contextlib import closing
from functools import partial
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Iterable, List, Optional, Dict


from .data import Fastq, SequencingData
from .operations.operation import Operation
from functools import partial

from abc import ABC, abstractmethod
logger = logging.getLogger("Pipeline")

class Pipeline(ABC):
    def __init__(self, temp_dir="/tmp"):     
        self._operations: List[Operation] = []    
        temp_dir = Path(temp_dir) if temp_dir else None
        if temp_dir and not temp_dir.is_dir():
            raise ValueError("Temporary directory path is not a directory.")
        else:
            self._temp_dir = temp_dir


    def add_operation(self, operation: Operation) -> None:
        """Schedule an operation in the pipeline. 
           Operations will be added at the end of the execution queue. 
        
        :param operation: The operation to be scheduled for execution.
        :type operation: Operation
        """
        self._operations.append(operation)

    @abstractmethod
    def execute(seq_data: SequencingData) -> None:
        """Excude the scheduled queue of operations.
           Operations are performed on a FIFO-basis.
        
        :param seq_data: The data that is used as the input for the first operation.
        :type seq_data: SequencingData
        """       
        raise NotImplementedError

class SerialPipeline(Pipeline):
    def execute(self, seq_data: SequencingData) -> None:
        """Excude the scheduled queue of operations.
           Operations are performed on a FIFO-basis.
        
        :param seq_data: The data that is used as the input for the first operation.
        :type seq_data: SequencingData
        """        
        for operation in self._operations:
            logger.info(f'Performing operation {operation}')
            seq_data = seq_data.edit(operation)

class MultiProcessPipeline(Pipeline):
    """A pipeline schedules operations that are performed on sequencing data for execution.

    `Pipeline` stores the operations to be performed on sequencing data 
    and allows for sequential execution of these operations. Output from one operation
    is passed on as input for the next operation.
    

    :param cpu: Number of cores or threads the operations scheduled in the pipeline 
                are allowed to use. The maximum amount of cores 
                or threads is limited to the value provided., defaults to 1
    :type cpu: int, optional
    :param temp_dir: Path to a directory where temporary files will be placed., defaults to "/tmp"
    :type temp_dir: str, optional
    :raises ValueError: The amnount of cores/threads defined by the user could not be interpreted as an integer.
    :raises ValueError: More maximum cores/threads were requested then available by on the system, 
                        being the cpu count reduced by 1 or 1 for single-core systems.
    :raises ValueError: Temporary directory path is not a directory or does not exits.
    """

    def __init__(self, cpu: int=1, temp_dir="/tmp"):        
        try:
            cpu = int(cpu)
        except ValueError:
            raise ValueError("The amnount of cores/threads to use should be an integer.")
        else:
            max_proc = multiprocessing.cpu_count()-1 or 1
            if cpu > max_proc:
                raise ValueError("Please leave at least one processing unit available for the system.")            
            self._cpu = cpu
        

        super().__init__(temp_dir)
        logger.debug(f"Created parallel pipeline with options cpu: {self._cpu}, temporary directory: {self._temp_dir}")


    def execute(self, seq_data: SequencingData) -> None:
        """Excude the scheduled queue of operations.
           Operations are performed on a FIFO-basis.
        
        :param seq_data: The data that is used as the input for the first operation.
        :type seq_data: SequencingData
        """
        with TemporaryDirectory(dir=self._temp_dir) as temp_dir, multiprocessing.Pool(self._cpu) as p:
            # Decompress, keep track of what samples were compressed
            compressed_runs = dict()
            uncompressed_fastqs = []
            for fastq in seq_data:
                if fastq.compressed:
                    compressed_runs[fastq.run] = fastq.compression
                    uncompressed_fastqs.append(fastq.decompress(temp_dir, threads=self._cpu))
                else:
                    uncompressed_fastqs.append(fastq)
            seq_data = SequencingData(uncompressed_fastqs)
            
            # Perform operations
            for operation in self._operations:
                previous_seq = seq_data
                if operation.supports_multiprocessing():
                    operation.cores = self._cpu
                    seq_data = seq_data.edit(operation)
                elif len(seq_data) >= self._cpu: # No split needed:
                    seq_data = seq_data.edit_parallel(p, operation, self._cpu)
                else: # Split needed 
                    split_seq_data = seq_data.split(self._cpu, temp_dir)
                    result_split = split_seq_data.edit_parallel(p, operation, cpu=self._cpu)
                    seq_data = result_split.join(operation.output_file_name_template)

                if compressed_runs:
                    self._background_compress(p, compressed_runs, previous_seq)
            self._background_compress(p, compressed_runs, seq_data)
    
    def _background_compress(self, pool: multiprocessing.Pool, 
                                   compressed_runs: Dict[str, str],
                                   seq_data: SequencingData):
        for fastq in seq_data:
            try:
                compression_suffix = compressed_runs[fastq.run]
            except KeyError:
                pass
            else:
                result = pool.apply_async(self._compression_worker, (fastq, compression_suffix)).get()
                fastq.remove()

    @staticmethod
    def _compression_worker(fastq: Fastq, compression_suffix: str):
        return fastq.compress(compression_suffix=compression_suffix)


                
    

