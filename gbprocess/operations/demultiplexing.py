"""
Operations to perform demultiplexing.
"""

import logging
from abc import ABC
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Dict, Union

from Bio import Seq, SeqIO, SeqRecord

from ..data import SequencingData, Fastq

from ..utils import CommandLineWrapper, fasta_to_dict
from .operation import Operation, register

LOGGER = logging.getLogger("Demultiplexing")

class Demultiplexer(Operation, ABC):
    pass

@register("CutadaptDemultiplex")
class CutadaptDemultiplex(CommandLineWrapper, Demultiplexer):
    """
    Perform sequence demultiplexing using cutadapt.

    Split a .fastq file into one or more samples based on the presence of
    'barcodes': a short sequence of nucleotides that is uniquely linked
    to one sample. Barcodes can be 'anchored', meaning that they will only
    be looked for at the beginning of the sequence.
    Additionally, in case of GBS data, the barcode sequence is followed by
    a sequence that is the remnant of the restriction site that was targeted
    to remove the sequence from the genome. Specifying the cutsite remnant
    improves the demultiplexing specificity, as there is more sequence to
    match with.

    This operation adds the {sample_name} file name template.

    :param error_rate: relative amount of allowed mismatches for a barcode or
        barcode+remnant combination to be matched (between 0 and 1).
    :type error_rate: float
    :param barcodes: path to an existing barcodes .fasta file, where the name
        of the fasta records indicates the sample name, and the sequences
        define the nucleotide sequence of sample specific barcodes.
    :type barcodes: Union[Path, str]
    :param output_file_name_template: Template that will be used to determine
        the name of the output files. The output file name template follow the
        syntax of format strings as descibed in
        `PEP 3101 <https://www.python.org/dev/peps/pep-3101/#id17/>`__. The
        template consists of text data that is transferred as-is to the output
        file name and replacement fields (indicated by curly braces) that
        describe what should be inserted in place of the field.The field name,
        the element inside the curly braces of the replacement field, must
        refer to a property attribute of the .fastq file if perform() is called.
    :type output_file_name_template: str
    :param output_directory: Path to an existing directory that will hold the
        output for this operation.
    :type output_directory: Union[Path, str]
    :param barcode_side_cutsite_remnant: String that describes the nucleotide
        sequence of the barcode side cutsite remnant., defaults to ""
    :type barcode_side_cutsite_remnant: str, optional
    :param anchored_barcodes: Only look for the barcode sequence
        (or barcode + restriction site remnant combination) at the beginning
        of the sequences., defaults to True
    :type anchored_barcodes: bool, optional
    :raises ValueError: error_rate could not be interpreted as a floating point
        number.
    :raises AssertionError: The error_rate must have a value between
        0 (incl.) and 1 (excl.).
    :raises ValueError: The fasta file containing the barcodes does not exist
        or is not a file.
    :raises ValueError: The barcode side cutsite remnant sequence must only
        contain IUPAC nucleotide characters.
    """

    def __init__(self,
                 error_rate: float,
                 barcodes: Union[Path, str],
                 output_file_name_template: str,
                 output_directory: Union[Path, str],
                 barcode_side_cutsite_remnant="",
                 anchored_barcodes=True):
        try:
            error_rate = float(error_rate)
        except ValueError:
            raise ValueError("The error rate must be a floating number.")
        else:
            assert 0 <= error_rate < 1, ("The error rate must have a value",
                                         "between 0 (incl.) and 1 (excl.).")

        barcodes = Path(barcodes)
        if not barcodes.is_file:
            raise ValueError("The fasta file containing the barcodes does " +
                             "not exist or is not a file.")
        self._check_nucleotides(barcode_side_cutsite_remnant)
        self._barcode_side_cutsite_remnant = str(barcode_side_cutsite_remnant)
        self._error_rate = error_rate
        self._barcodes_dict = fasta_to_dict(barcodes)
        self._anchored_barcodes = anchored_barcodes
        self._barcodes_with_re = self._combine_barcodes_with_restriction_site(
            self._barcodes_dict, self._barcode_side_cutsite_remnant
            )

        CommandLineWrapper.__init__(self, 'cutadapt')
        Demultiplexer.__init__(self, output_file_name_template, output_directory)

    def perform(self, fastq: Fastq) -> SequencingData:
        """Perform demultiplexing on the target .fastq sample.

        :param fastq: Input .fastq file(s). Both single-end and paired-end
            sequencing types are accepted. The Property attributes of the
            Fastq object that correspond to the field names of the output
            file name template, must be defined.
        :type fastq: Fastq
        :raises ValueError: This operation requires one or two .fastq files
            are input, but more were specified.
        :return: A new SequencingData object, which specifies a list of
            samples that are the result of the demultiplexing.
        :rtype: SequencingData
        """
        LOGGER.debug("Processing %r." % fastq)
        if len(fastq.files) > 2:
            raise ValueError("Cutadapt requires one or two input files.")

        output_fastq = [
            fastq.create_from_properties(
                self._output_file_name_template,
                self._output_directory,
                orientation=fastq.orientation,
                run=fastq.run,
                extension=fastq.extension,
                sample_name=sample_name)
            for sample_name in self._barcodes_dict.keys()]
        LOGGER.debug("Created output fastq files %s.", str(output_fastq))

        if fastq.empty:
            LOGGER.debug("Input fastq is empty, returning empty files.")
            return SequencingData(output_fastq)

        output_template_for_cutadapt = self._output_file_name_template.replace(
            '{sample_name}', '{{name}}')
        filled_output_template = fastq.fill_template_to_path(
            output_template_for_cutadapt,
            orientation=fastq.orientation,
            run=fastq.run,
            extension=fastq.extension)

        output_file_names_arg = [part
                                 for arg_ in
                                 list(zip(["-o", "-p"], filled_output_template))
                                 for part in arg_]
        with NamedTemporaryFile("w") as temp_barcodes_fasta:
            LOGGER.debug("Barcodes fasta: %s", temp_barcodes_fasta.name)
            LOGGER.debug("Length of barcodes fasta dict: %s", len(self._barcodes_with_re))
            SeqIO.write(self._barcodes_with_re.values(), temp_barcodes_fasta, 'fasta')
            temp_barcodes_fasta.flush()
            LOGGER.debug("Barcodes fasta file flushed.")
            args = [
                "-e", str(self._error_rate),
                "--no-indels",
                "--action=none",
                "-g", "file:{}".format(str(temp_barcodes_fasta.name)),
                "--compression-level=9"
            ]
            args.extend(output_file_names_arg)
            args.extend([str(file_) for file_ in fastq.files])
            super().run(*args, working_directory=self._output_directory)
        seq_data = SequencingData(output_fastq)
        return seq_data

    def _combine_barcodes_with_restriction_site(self,
                                                barcodes_dict: dict,
                                                restriction_site_sequence: str)\
                                                -> Dict[str, SeqRecord.SeqRecord]:
        """
        For every barcode in the barcodes fasta, add the restriction site as
        a suffix. Additionally, if the barcodes should be anchored, add '^'
        to the beginning of each nucleotide sequence.
        """
        result = dict()
        for sample_name, barcode_sequence in barcodes_dict.items():
            combined = barcode_sequence + restriction_site_sequence
            final_sequence = f"^{combined}" if self._anchored_barcodes else combined
            result[sample_name] = SeqRecord.SeqRecord(
                Seq.Seq(final_sequence),
                id=sample_name,
                name=sample_name,
                description=sample_name)
            if len(combined) < 10:
                LOGGER.warning("The length of the sequence used for" +
                               "demultiplexing sample %s is potentially" +
                               "too small (%s).",
                               sample_name,
                               len(barcode_sequence + restriction_site_sequence))
        LOGGER.debug("Barcodes used: %s", result)
        return result

    def supports_multiprocessing(self):
        """This operation does not support multiprocessing."""
        return False
