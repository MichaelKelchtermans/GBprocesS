"""
Operations to perform read trimming.
"""
import logging
from abc import ABC, abstractclassmethod, abstractmethod
from pathlib import Path
from typing import Union

from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq

from ..data import Fastq, SequencingData
from ..utils import CommandLineWrapper, fasta_to_dict
from .operation import Operation, register

logger = logging.getLogger("Trimming")

class Trimmer(Operation, ABC):
    pass

@register("CutadaptPositionalTrimmer")
class CutadaptPositionalTrimmer(CommandLineWrapper, Trimmer):
    """
    Perform positional trimming: the removal of a fixed-number of bases from
    reads.

    Schematic of sequenced reads in a GBS experiment::
    
        ...|--adapter--|-barcode-|-RE-|-genomic insert-|-RE-|--adapter--|...
               -primer->                                    <-primer-

        Forward-read   
        (or merged or
        single-end):
                       |-barcode-|-RE-|-genomic insert-|-RE-|--adapter--|...

        Reverse-read: 
        ...|--adapter--|-barcode-|-RE-|-genomic insert-|-RE-|

    This operation is especially taylored for GBS applications where there is a 
    need to remove the barcode and restriction enzyme cutsite remnant sequences
    from the 5' side of forward reads, while also removing restriction enzyme
    cutsite remnants from the 5' side of reverse-oriented reads.
    Positional trimming is not performed differently for single-end (or merged)
    and paired end-sequencing. That is, single-end (or merged reads) are treated
    as if they would be forward reads from paired-end samples. 
    
    The amount of bases removed from the reads is determined by the length of
    sample-specific barcode, the length of the largest barcode across all
    samples being processed and the length of the restriction enzyme cutsite
    remnants. The cutsite remnant can be deduced from the full site, see the 
    following example where the '|' symbol indicate where the endonuclease
    would cut the nucletode sequence (`MspI 
    <https://international.neb.com/products/r0106-mspi>`__ and `ApaI 
    <https://international.neb.com/products/r0114-apai>`__)::
    	
        #MspI
        5'--C|CGG--3'
        3'--GGC|C--5'
        Restriction enzyme cutsite remnant: CGG

        #ApaI
        5'--GGGCC|C--3'
        3'--C|CCGGG--5'
        Restriction enzyme cutsite remnant: GGGCC

    Depending on the protocol, one (single-digest) or two (double-digest)
    restriction enzymes may have been used to retreive the genomic fragments.
    With the latter situation, one of the remnants resides at the 5' prime end 
    of the genomic insert, next to the barcode sequence, while the other will
    be present at the 3' side. The 5' cutsite remnant is called the 
    'barcode side' cutside remnant, while 3' cutsite remnant is called the
    'common side' cutsite remnant. In case a single-digest protocol,
    the same restriction enzyme sequence can be used to define both the common
    side cutsite remnant and the barcode site cutsite remnant.

    For the reverse-oriented reads, only the 5' end is trimmed to remove the
    restriction site cutsite remnant. The 5' end of forward-oriented reads is
    trimmed to remove a number of bases equal to the combined length of the
    sample-specific barcode and the restriction enzyme cutsite remnant.
    Additionally, the forward read is trimmed at the 3' end, where a number
    of nulcleotides is removed equal to the length of the largest barcode
    across all samples, reduced with the number of nulceotides in the barcode
    of the current sample. This means that the number of nucleotides removed
    from the forward reads is always the same across all samples, regardless
    whether there is a difference is barcode length between the samples.

    :param barcode_side_cutsite_remnant: Nucleotide sequence of the barcode
        side cutsite remnant.
    :type barcode_side_cutsite_remnant: str
    :param common_side_cutsite_remnant: Nucleotide sequence of the common
        side cutsite remnant. For single-end or merged reads, this is the
        same sequence as defined in ``barcode_side_cutsite_remnant``.
    :type common_side_cutsite_remnant: str
    :param barcodes: path to an existing barcodes .fasta file, where the name
        of the fasta records indicates the sample name, and the sequences
        define the nucleotide sequence of sample specific barcodes.
    :type barcodes: Union[Path, str]
    :param output_file_name_template: Template that will be used to determine
        the name of the output files. The output file name template follow the
        syntax of format strings as descibed in
        `PEP 3101 <https://www.python.org/dev/peps/pep-3101/#id17/>`__. The
        template consists of text data that is transferred as-is to the output
        file name and replacement fields (indicated by curly braces) that
        describe what should be inserted in place of the field.The field name,
        the element inside the curly braces of the replacement field, must
        refer to a property attribute of the .fastq file if perform() is called.
    :type output_file_name_template: str
    :param output_directory: Path to an existing directory that will hold the
        output for this operation.
    :type output_directory: Union[Path, str]
    :raises ValueError: The adapters file does not exist or is not a file.

    """    
    def __init__(self,
                    barcode_side_cutsite_remnant: str,
                    common_side_cutsite_remnant: str,
                    barcodes: Union[Path, str],
                    output_file_name_template: str,
                    output_directory: Union[Path, str]):     
        #TODO: check input even more

        barcodes = Path(barcodes)
        if not barcodes.is_file():
            raise ValueError("The adapters file does not exist or is not a file.")
        
        self._barcode_side_cutsite_remnant = barcode_side_cutsite_remnant
        self._common_side_cutsite_remnant = common_side_cutsite_remnant
        self._barcodes = fasta_to_dict(barcodes)
        logger.debug(f"Created positional trimming operation with options:" + 
                f"Cutsite remnant at barcode side: {self._barcode_side_cutsite_remnant}," +
                f"Cutsite remnant at the common side: {self._common_side_cutsite_remnant}," +
                f"Fasta file containing barcodes per sample: {self._barcodes}," +
                f"Output file name template: {output_file_name_template}," +
                f"Output directory: {output_file_name_template}")

        CommandLineWrapper.__init__(self, 'cutadapt')
        Trimmer.__init__(self, output_file_name_template, output_directory)

    def perform(self, fastq: Fastq):
        barcode = self._get_barcode(fastq)
        fastq = self._positional_trimming(fastq, barcode)
        seq_data =  SequencingData([fastq])
        return seq_data

    def _get_barcode(self, fastq: Fastq):
        sample_name = fastq.sample_name
        try:
            barcode = self._barcodes[sample_name]
            logger.debug(f"Selected barcode: {barcode}")
            return barcode
        except KeyError:
            raise ValueError('Barcode {} not found in barcodes file.'.format(sample_name))

    def _positional_trimming(self, fastq: Fastq, barcode: str):
        files = [file_.resolve() for file_ in fastq.files]
        output_fastq = fastq.create_from_properties(self._output_file_name_template,
                                                self._output_directory,
                                                orientation = fastq.orientation, 
                                                run = fastq.run,
                                                extension = fastq.extension,
                                                sample_name = fastq.sample_name
                                               )
        output_files = [file_.name for file_ in output_fastq.files]
        logger.debug(f"Created output fastq files: {output_fastq}")
        if fastq.empty:
            return output_fastq

        output_file_arg = [part for arg_ in list(zip(["-o", "-p"], output_files)) for part in arg_]
        largest_barcode = max(self._barcodes.values(), key=len)
        args = [
            "-u", len(barcode) + len(self._barcode_side_cutsite_remnant),
            "-u", -(len(largest_barcode) - len(barcode))
            #"-j", self.cores
            # "-U", currently not needed to remove something from 3' of the reverse read.
            # Will be needed if we handle spacers! 
        ]
        if len(output_files) == 2:
            args.extend(["-U", len(self._common_side_cutsite_remnant) + 1])

        args.extend(output_file_arg)
        args.extend(files)
        super().run(*args, working_directory=self._output_directory)
        return output_fastq

    def supports_multiprocessing(self):
        return False # Cutadapt multiprocessing is slow

@register("CutadaptPatternTrimmer")
class CutadaptPatternTrimmer(CommandLineWrapper, Trimmer):
    """Perform pattern trimming: the removal of nucleotides from the
    beginning and end of fastq records based on pattern recognition.

    Pattern trimming is a custom-build operation to remove to remove 
    barcode, restriction enzyme cutsite remnants and adapter sequences
    from reads in GBS experiments. It is similar to 
    :class:`.CutadaptPositionalTrimmer`, but is sequence-specific, while
    positional trimming performd trimming removes a number nucleotides from
    reads regardless of which nucleotides they are. Pattern trimming is not
    performed differently for single-end (or merged) and paired
    end-sequencing. That is, single-end (or merged reads) are treated as
    if they would be forward reads from paired-end samples. 

    Schematic of sequenced reads in a GBS experiment::
    
        ...|--adapter--|-barcode-|-RE-|-genomic insert-|-RE-|--adapter--|...
               -primer->                                    <-primer-

        Forward-read   
        (or merged or
        single-end):
                       |-barcode-|-RE-|-genomic insert-|-RE-|--adapter--|...

        Reverse-read: 
        ...|--adapter--|-barcode-|-RE-|-genomic insert-|-RE-|

    Pattern trimming edits both the forward and the reverse reads at the 3'
    end. For reserve reads, a restriction site remnant may be present,
    followed by the sample specific barcode and an adapter sequence. 
    Depending on the read length, these sequences will present in full to even
    not at all. With pattern trimming, the reads are trimmed even if the 
    sequences are only partially present. This is why we can not use the 
    same mechanic as positional trimming 
    (see :class:`.CutadaptPositionalTrimmer`). The adapter sequence and
    restriction enzyme cutsite remnant which are present next to the
    barcode are called the barcode-side restriction enzyme cutsite remant
    and the barcode-side adapter sequence. On the other hand, an adapter 
    sequence and a restriction enzyme cutsite remnant may also be present
    at the 3' in the forward read. In this case, as these sequences do not
    flank a barcode, they are called the common-side adapter sequence and
    the common-side restriction enzyme cutsite remnant.

    In order to perform pattern trimming, the sequences of the restriction
    enzyme cutsite remants, the adapter sequences and the barcode must be
    provided. The sequences must be provided in 5' to 3' orientation as present
    in the sequenced template. This means, that for the forward reads, the 
    combined sequences of the common-side restriction enzyme cutsite remant and
    the common-side adapter will be searched for in reverse complement.
    Additionally, in the reverse reads, the barcode-side cutsite remnant is
    combined with the reverse complement of the barcode and the barcode-side
    adapter sequence. This means that the adapter sequences can be provided as
    described in the manual of the sequence provider. The cutsite remnant can 
    be deduced from the full site, see the following example where the '|'
    symbol indicate where the endonuclease would cut the nucletode sequence
    (`MspI 
    <https://international.neb.com/products/r0106-mspi>`__ and `ApaI 
    <https://international.neb.com/products/r0114-apai>`__)::
    	
        #MspI
        5'--C|CGG--3'
        3'--GGC|C--5'
        Restriction enzyme cutsite remnant: CGG

        #ApaI
        5'--GGGCC|C--3'
        3'--C|CCGGG--5'
        Restriction enzyme cutsite remnant: GGGCC

    Depending on the protocol, one (single-digest) or two (double-digest)
    restriction enzymes may have been used to retreive the genomic fragments.
    With the latter situation, one of the remnants resides at the 5' prime end 
    of the genomic insert, next to the barcode sequence, while the other will
    be present at the 3' side. In case a single-digest protocol,
    the same restriction enzyme sequence can be used to define both the common
    side cutsite remnant and the barcode site cutsite remnant.

    :param barcode_adapter_sequence: Nucleotide sequence of the barcode-
        side adapter sequence.
    :type barcode_adapter_sequence: str
    :param common_adapter_sequence: Nucleotide sequence of the common-
        side adapter sequence.
    :type common_adapter_sequence: str
    :param barcode_side_cutsite_remnant: Nucleotide sequence of the barcode-
        side cutsite remnant.
    :type barcode_side_cutsite_remnant: str
    :param common_side_cutsite_remnant: Nucleotide sequence of the common-
        side cutsite remnant.
    :type common_side_cutsite_remnant: str
    :param barcodes: path to an existing barcodes .fasta file, where the name
        of the fasta records indicates the sample name, and the sequences
        define the nucleotide sequence of sample specific barcodes.
    :type barcodes: Union[Path, str]
    :param minimum_length: Minimum length of the reads after trimming, shorter
        reads are discarded from the output.
    :type minimum_length: int
    :param error_rate: Amount of errors allowed between query sequences and the
        reads. Expressed as an floating point number from the interval ]0,1].
    :type error_rate: float
    :param output_file_name_template: Template that will be used to determine
        the name of the output files. The output file name template follow the
        syntax of format strings as descibed in
        `PEP 3101 <https://www.python.org/dev/peps/pep-3101/#id17/>`__. The
        template consists of text data that is transferred as-is to the output
        file name and replacement fields (indicated by curly braces) that
        describe what should be inserted in place of the field.The field name,
        the element inside the curly braces of the replacement field, must
        refer to a property attribute of the .fastq file if perform() is called
    :type output_file_name_template: str
    :param output_directory: Path to an existing directory that will hold the
        output for this operation.
    :type output_directory: Union[Path, str]
    :raises ValueError: The minimum length of the trimmed read must be 
        interpretable as an integer.
    :raises ValueError: The minimum length of the trimmed read must be 
        an integer above 0.
    :raises ValueError: The error rate must be interpretable as a 
        float.
    :raises ValueError: The common side adapter sequence and the common
        side cutsite remnant must be defined.
    :raises ValueError: If either the barcode side adapter sequence or the
        barcode side cutsite remnant are specified, the other one must be
        defined as well.
    :raises ValueError: The path to the barcodes file does not exist or points
        to something that is not a file.
    :raises ValueError: The error rate must have a value larger than 0 and
        smaller than one (]0,1]).
    """
    def __init__(self, barcode_adapter_sequence: str,
                       common_adapter_sequence: str,
                       barcode_side_cutsite_remnant: str,
                       common_side_cutsite_remnant: str,
                       barcodes: Union[Path, str], 
                       minimum_length: int, 
                       error_rate: float, 
                       output_file_name_template: str,
                       output_directory: Union[Path, str]):  
        #TODO: check input even more
        try:
            minimum_length = int(minimum_length)
        except ValueError:
            raise ValueError("The minimum length must be an integer.")

        if minimum_length <= 0:
            raise ValueError("The minimum length must have a value larger than 0.")

        try:
            error_rate = float(error_rate)
        except ValueError:
            raise ValueError("The error rate length must be float.")

        if 0 > error_rate <= 1:
            raise ValueError("The error rate must have a value larger than 0 and smaller than one (]0,1]).")

        barcodes = Path(barcodes)
        if not barcodes.is_file():
            raise ValueError("The barcodes file does not exist or is not a file.")
        
        # self._input_file_names = input_file_name_template
        self._barcode_adapter_sequence = barcode_adapter_sequence.strip('"\'')
        self._common_adapter_sequence = common_adapter_sequence.strip('"\'')
        self._barcode_side_cutsite_remnant = barcode_side_cutsite_remnant.strip('"\'')
        self._common_side_cutsite_remnant = common_side_cutsite_remnant.strip('"\'')
        if not self._common_adapter_sequence or not self._common_side_cutsite_remnant:
           raise ValueError('The common side adapter sequence and the barcode' +
                            'side cutsite remnant must be defined.')
        if (self._barcode_adapter_sequence and not self._barcode_side_cutsite_remnant) or \
           (self._barcode_side_cutsite_remnant and not self._barcode_adapter_sequence):
           raise ValueError('If either the barcode side adapter sequence or the barcode' +
                            'side cutsite remnant are specified (paired-end sequences), ' +
                            'the other one must be defined as well.')
        self._barcodes = fasta_to_dict(barcodes)
        self._minimum_length = minimum_length
        self._error_rate = error_rate
        CommandLineWrapper.__init__(self, 'cutadapt')
        Trimmer.__init__(self, output_file_name_template, output_directory)

    def perform(self, fastq: Fastq):
        barcode = self._get_barcode(fastq)
        fastq =  self._pattern_trimming(fastq, barcode)
        seq_data =  SequencingData([fastq])
        return seq_data

    def _pattern_trimming(self, fastq: Fastq, barcode: str):
        files = [file_.resolve() for file_ in fastq.files]
        output_fastq = fastq.create_from_properties(self._output_file_name_template,
                                self._output_directory,
                                orientation = fastq.orientation, 
                                run = fastq.run,
                                extension = fastq.extension,
                                sample_name = fastq.sample_name
                                )
        if fastq.empty:
            return output_fastq
        patterns = self._build_patterns(barcode)
        output_files = [file_.name for file_ in output_fastq.files]
        combination_args = [part for arg_ in list(zip(["-a", "-A"], patterns)) for part in arg_]
        output_file_arg = [part for arg_ in list(zip(["-o", "-p"], output_files)) for part in arg_]
        args = [
            "--minimum-length", self._minimum_length
            #"-j", self.cores
        ]
        args.extend(output_file_arg)
        args.extend(combination_args)
        args.extend(files)
        super().run(*args, working_directory=self._output_directory)
        return output_fastq

    def _get_barcode(self, fastq: Fastq):
        # Get a list of possible file names
        sample_name = fastq.sample_name
        try:
            return self._barcodes[sample_name]
        except KeyError:
            raise ValueError('Barcode {} not found in barcodes file.'.format(sample_name))

    def _build_patterns(self, barcode):
        result = []
        result.append(self._reverse_complement(self._common_side_cutsite_remnant + self._common_adapter_sequence))
        if self._barcode_adapter_sequence:
            result.append(self._barcode_side_cutsite_remnant + \
                          self._reverse_complement(barcode) + \
                          self._reverse_complement(self._barcode_adapter_sequence))
        return result

    @staticmethod
    def _reverse_complement(dna):
        seq = Seq(dna, generic_dna)
        return str(seq.reverse_complement())

    def supports_multiprocessing(self):
        return False # Cutadapt multiprocessing is slow
