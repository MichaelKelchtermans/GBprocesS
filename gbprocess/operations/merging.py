"""
Operations to merge paired-end .fastq files.
"""
import logging
from abc import ABC, abstractclassmethod, abstractmethod
from os.path import commonprefix
from pathlib import Path
from typing import Union

from ..data import SequencingData, Fastq, SingleEndFastq
from ..utils import CommandLineWrapper
from .operation import Operation, register

logger = logging.getLogger("Merging")

class Merger(Operation, ABC):
    pass
    
@register("Pear")
class Pear(CommandLineWrapper, Merger):
    def __init__(self, minimum_overlap: int, minimum_length: int, output_file_name_template: str, output_directory: Union[Path, str]):
        try:
            minimum_overlap = int(minimum_overlap)
        except ValueError:
            raise ValueError("The minimum overlap must be an integer.")

        if minimum_overlap <= 0:
            raise ValueError("The minimum overlap must have a value larger than 0.")

        try:
            minimum_length = int(minimum_length)
        except ValueError:
            raise ValueError("The minimum length must be an integer.")

        if minimum_length <= 0:
            raise ValueError("The minimum length must have a value larger than 0.")
        
        if not output_file_name_template.endswith('.assembled{extension}'):
            raise ValueError("The output files from PEAR end with '.assembled{extension}'." +
                             "Please specify this in the template for the output files in the configuration.")

        self._minimum_overlap = minimum_overlap
        self._minimum_length = minimum_length

        logger.debug(f"Creating Pear merge operation with options:" + 
                     f"output directory: {output_directory}," +
                     f"output file name template: {output_file_name_template}," + 
                     f"minimum overlap between reads: {self._minimum_overlap}," +
                     f"minimum read length: {self._minimum_length}")

        CommandLineWrapper.__init__(self, 'pear')
        Merger.__init__(self, output_file_name_template, output_directory)
        
    def perform(self, fastq: Fastq):
        if len(fastq.files) != 2:
            raise ValueError("Merging can only occur between to files.")
        #TODO: compressed files need to be decompressed
        forward, reverse = [file_.resolve() for file_ in fastq.files]
        output_fastq = SingleEndFastq.create_from_properties(self._output_file_name_template,
                                        self._output_directory,
                                        run = fastq.run,
                                        extension = ".fastq",
                                        sample_name = fastq.sample_name,
                                        orientation = ("3",)
                                        )
        logger.debug(f"Created output fastq files {output_fastq}")
        if fastq.empty:
            return SequencingData([output_fastq])
        output_file = output_fastq.files[0]
        output_prefix_template = output_file.name[:-len(".assembled.fastq")]

        args = [
            "-f", str(forward),
            "-r", str(reverse),
            "-v", str(self._minimum_overlap),
            "-n", str(self._minimum_length),
            "-o", output_prefix_template,
            "-j", str(self._cores)
        ]
        super().run(*args, working_directory=self._output_directory)

        if fastq.compressed:
            output_fastq = output_fastq.compress(self._output_directory, fastq.compression)

        return SequencingData([output_fastq])
    
    def supports_multiprocessing(self):
        return True

@register("FastqJoinMerger")
class FastqJoinMerger(CommandLineWrapper, Merger):
    def __init__(self, minimum_overlap: int, percent_maximum_difference: int, output_file_name_template: str, output_directory: Union[Path, str]):
        try:
            minimum_overlap = int(minimum_overlap)
        except ValueError:
            raise ValueError("The minimum overlap must be an integer.")

        if minimum_overlap <= 0:
            raise ValueError("The minimum overlap must have a value larger than 0.")

        try:
            percent_maximum_difference = int(percent_maximum_difference)
        except ValueError:
            raise ValueError("The maximum difference must be an integer.")

        if percent_maximum_difference <= 0 or percent_maximum_difference > 100:
            raise ValueError("The maximum difference must have a value in ]0,100]")

        self._minimum_overlap = minimum_overlap
        self._percent_maximum_difference = percent_maximum_difference

        logger.debug(f"Creating fastq-join merge operation with options:" + 
                     f"output directory: {output_directory}," +
                     f"output file name template: {output_file_name_template}," + 
                     f"percentage maximum difference: {self._percent_maximum_difference}")

        if not output_file_name_template.endswith('.join{extension}'):
            raise ValueError("The output files from PEAR end with '.join{extension}'." +
                             "Please specify this in the template for the output files in the configuration.")

        CommandLineWrapper.__init__(self, 'fastq-join')
        Merger.__init__(self, output_file_name_template, output_directory)

    def perform(self, fastq: Fastq):
        if len(fastq.files) != 2:
            raise ValueError("Merging can only occur between to files.")
        forward, reverse = [file_.resolve() for file_ in fastq.files]
        output_fastq = SingleEndFastq.create_from_properties(self._output_file_name_template,
                                    self._output_directory,
                                    run = fastq.run,
                                    extension = fastq.extension,
                                    sample_name = fastq.sample_name
                                    )
        logger.debug(f"Created output fastq files {output_fastq}")

        if fastq.empty:
            return SequencingData([output_fastq])

        output_file = output_fastq.files.pop()
        output_prefix_template = output_file.name[:-len(".join{extension}")]

        args = [
            f"-p{str(self._percent_maximum_difference)}",
            f"-m{str(self._minimum_overlap)}",
            str(forward),
            str(reverse),
            "-o", f"{output_prefix_template}.%{fastq.extension}"
        ]
        super().run(*args, working_directory=self._output_directory)
        return SequencingData([output_fastq])


    def supports_multiprocessing(self):
        return False


@register("FastpMerger")
class FastpMerger(CommandLineWrapper, Merger):
    def __init__(self, minimum_overlap: int, output_file_name_template: str, output_directory: Union[Path, str]):
        

        logger.debug(f"Creating fastq-join merge operation with options:" + 
                     f"output directory: {output_directory}," +
                     f"output file name template: {output_file_name_template}")

        if not output_file_name_template.endswith('.join{extension}'):
            raise ValueError("The output files from Fastp end with '.join{extension}'." +
                             "Please specify this in the template for the output files in the configuration.")

        CommandLineWrapper.__init__(self, 'fastp')
        Merger.__init__(self, output_file_name_template, output_directory)

    def perform(self, fastq: Fastq):
        if len(fastq.files) != 2:
            raise ValueError("Merging can only occur between to files.")
        forward, reverse = [file_.resolve() for file_ in fastq.files]
        output_fastq = SingleEndFastq.create_from_properties(self._output_file_name_template,
                                    self._output_directory,
                                    run = fastq.run,
                                    extension = fastq.extension,
                                    sample_name = fastq.sample_name
                                    )
        logger.debug(f"Created output fastq files {output_fastq}")

        if fastq.empty:
            return SequencingData([output_fastq])

        output_file = output_fastq.files.pop()
        output_template = output_file.name[:-len(".join{extension}")]

        args = [
            '-i', str(forward),
            '-I', str(reverse),
            '--out1', f'{output_template}.out1{fastq.extension}',
            '--out2', f'{output_template}.out2{fastq.extension}',
            '--unpaired1', f'{output_template}.unpaired1{fastq.extension}',
            '--unpaired2', f'{output_template}.unpaired2{fastq.extension}',
            '--merged_out', output_file.name,
            '--disable_adapter_trimming',
            '--disable_length_filtering',
            '--disable_trim_poly_g',
            '--disable_quality_filtering',
            '-w', self._cores,
            '-m'
        ]
        super().run(*args, working_directory=self._output_directory)
        return SequencingData([output_fastq])


    def supports_multiprocessing(self):
        return True