import argparse
import logging
import sys
from collections import OrderedDict
from configparser import ConfigParser
from pathlib import Path
from typing import Union

from gbprocess.operations import *
from gbprocess.operations.operation import get_operation, _ALL_OPERATIONS
from gbprocess import __version__

from gbprocess.data import PairedEndFastq, SingleEndFastq, SequencingData
from gbprocess.pipeline import SerialPipeline, MultiProcessPipeline

def main(args=None):
    # Get the location and process the INI file.
    parser = argparse.ArgumentParser(prog="gbprocess")
    parser.add_argument("--config", "-c", nargs=1, help="INI file containing the pipeline's config", metavar='CONFIG')
    parser.add_argument("--debug", help="Enable verbose logging.", action="store_true")
    parser.add_argument("--operations", help="List the possible operations", action="store_true")
    parser.add_argument('--version', action='version', version=__version__)

    if args is None:
        args = sys.argv[1:]
    
    if len(args) < 1:
        parser.print_help()
        parser.exit(1)
        
    parsed_args = parser.parse_args(args)
    if parsed_args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
        #sys.tracebacklimit = 0 # Suppress traceback information on errors.
   

    logging.basicConfig(stream=sys.stdout, level=level, format='%(asctime)s %(name)s - %(levelname)s: %(message)s')
    logging.info(f"This is version {__version__}")
    
    if parsed_args.operations and parsed_args.config:
        logging.error("Both --config and --operations specified.")
        parser.print_help()
        parser.exit(1)
    elif parsed_args.operations:
        print("\n".join(_ALL_OPERATIONS.keys()))
    elif parsed_args.config:
        proces(parsed_args.config.pop())
    else:
        logging.error("Either --config or --operations must be specified.")
        parser.print_help()
        parser.exit(1)

def sequencingdata_from_dir(directory: Union[Path, str], sequencing_type: str, file_name_template: str):
    string_to_type = {'pe': PairedEndFastq, 'se': SingleEndFastq}
    try: 
        sequencing_type_o = string_to_type[sequencing_type]
    except KeyError:
        raise ValueError("The sequencing type must be a value from {}".format(",".join(string_to_type.keys())))
    else:
        return SequencingData.from_directory(sequencing_type_o, directory, file_name_template)

def proces(ini_file: Union[str, Path]):
    ini_file = Path(ini_file)
    logging.info("Parsing configuration file {}".format(ini_file))
    if not ini_file.is_file():
        raise ValueError("Config file does nog exist or is not a file.")
    config_parser = ConfigParser(dict_type=OrderedDict, strict=False)
    with ini_file.open('rt') as handler:
        config_parser.read_file(handler)
        logging.debug(f"Configuration: { {section: dict(config_parser[section]) for section in config_parser} }")

        try:
            seqs, empty_pipeline = parse_general_section(config_parser["General"])
        except KeyError:
            raise ValueError("Please define the 'General' section at the top of your configuration file")
        pipeline = parse_other_sections(config_parser, empty_pipeline)
    pipeline.execute(seqs)

def parse_general_section(arguments):
    logging.info(f"""General run information:\r
                            Input directory: {arguments.get("input_directory")}\r
                            Sequencing type: {arguments.get("sequencing_type")}\r
                            Input file template: {arguments.get("input_file_name_template")}
                            Cores/threads: {arguments.get('cores')}""")
            
    seqs = sequencingdata_from_dir(arguments["input_directory"], 
                                    arguments["sequencing_type"], 
                                    arguments["input_file_name_template"])
    try:
        cpu = int(arguments['cores'])
    except ValueError:
        raise ValueError('Could not interprete number of cpu cores as an integer.')
    else:
        if cpu == 1:
            pipeline = SerialPipeline(temp_dir=arguments.get("temp_dir"))
        else:
            pipeline = MultiProcessPipeline(cpu= cpu, temp_dir=arguments.get("temp_dir"))
    return seqs, pipeline

def parse_other_sections(config_parser, pipeline):
    for section, arguments in config_parser.items():
        if section in ("DEFAULT", "General"):
            pass
        else:
            operation = get_operation(section)(**dict(arguments))
            pipeline.add_operation(operation)
    return pipeline

if __name__ == '__main__':
    main()
