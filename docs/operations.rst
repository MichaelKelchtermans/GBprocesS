==========
Operations
==========

.. automodule:: gbprocess.operations
    :members:
    :exclude-members: supports_multiprocessing, perform