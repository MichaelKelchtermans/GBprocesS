GBS data preprocessing: How it works
====================================

Read preprocessing steps
------------------------

NGS read preprocessing requires specific steps for single-digest or double-digest GBS, 
in combination with single-end or paired-end sequenced reads.
Paired-end reads may be mapped separately or merged with (PEAR) before mapping.

This page provides the Quick start guidelines and examples of the config.ini files for six 
common scenarios using (a combination of) the following steps:

1. Demultiplexing (`Cutadapt <https://cutadapt.readthedocs.io/en/stable/>`_).
2. Trimming barcodes and restriction enzyme cutsite remnants at the 5’ end of the reads
   (`Cutadapt <https://cutadapt.readthedocs.io/en/stable/>`_).
3. Trimming of adapter sequences at the 3’ end of the reads (`Cutadapt <https://cutadapt.readthedocs.io/en/stable/>`_).
4. Merging of forward and reverse reads.
5. Removal of reads with low quality base-calling (Python).
6. Removal of reads with internal restriction sites (Python).

Barcodes and adapter sequences must be removed because these do not occur in the reference genome 
and would lead to errors in read mapping and/or identification of polymorphisms that do not exist.
Restriction site remnants are removed as these would create positional overlaps of neighboring stacks
derived from independent PCR-amplified GBS fragments. We recommend to remove reads with overall low 
quality and with internal restriction sites. Do `not` use clipping of reads with a sliding window that
clips reads when base calling quality within a window drops below a specified threshold, as this will
create reads with variable length, and thus mappings of variable length, which reflect technical artefacts
and not biologically meaningfull sequence diversity.



Single-digest GBS and single-end sequencing
-------------------------------------------

.. tabs::
   .. tab:: Scheme

      .. image:: images/separately_SE_SD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

        [General]
        cores = 32
        input_directory = /home/User/GBS_preprocessing
        sequencing_type = se
        input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
        # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
        # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
        # {:XX} mark the number of characters in the file name that contain the specified information.
        # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps. 

        [CutadaptDemultiplex]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        error_rate = 0
        output_directory = ./01_demultiplex
        output_file_name_template = {sample_name}_{orientation}{extension}
        barcode_side_cutsite_remnant = TGCAG
        # the (barcode + barcode_side_cutsite_remnant) sequence is searched per read, to increase specificity of demultiplexing.
        # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

        [CutadaptPositionalTrimmer]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        output_directory = ./02_positional_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # positional trimming only needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.  

        [CutadaptPatternTrimmer]
        common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
        # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
        # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        common_side_cutsite_remnant = TGCAG
        minimum_length = 60
        error_rate = 0
        output_directory = ./03_pattern_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}  
        # single-end sequencing always starts from the barcode adapter side (i5), and pattern trimming should remove the potential common adapter from the 3' of the Forward reads.  
        # in single-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is the same as on the barcode adapter side.  

        [MaxNFilter]
        max_n = 0 
        output_directory = ./04_max_n_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with N base calls.

        [SlidingWindowQualityFilter]
        window_size = 2
        average_quality = 20
        count = 1
        output_directory = ./05_sliding_window
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with low quality base calls.

        [AverageQualityFilter]
        average_quality = 25
        output_directory = ./06_average_quality_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes low quality reads.

        [RemovePatternFilter]
        pattern = CTGCAG
        output_directory = ./07_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.


Single-digest GBS, paired-end sequencing, merged reads
------------------------------------------------------

.. tabs::
   .. tab:: Scheme

      .. image:: images/separately_PE_SD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

        [General]
        cores = 32
        input_directory = /home/User/GBS_preprocessing
        sequencing_type = pe
        input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
        # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
        # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
        # {:XX} mark the number of characters in the file name that contain the specified information.
        # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps.

        [CutadaptDemultiplex]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        error_rate = 0
        output_directory = ./01_demultiplex
        output_file_name_template = {sample_name}_{orientation}{extension}
        barcode_side_cutsite_remnant = TGCAG
        # the (barcode + barcode_side_cutsite_remnant) sequence is searched per Forward read, to increase specificity of demultiplexing.
        # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

        [CutadaptPositionalTrimmer]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        common_side_cutsite_remnant = CTAA
        output_directory = ./02_positional_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # positional trimming needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.
        # positional trimming needs to know the length of the restriction enzyme cutside remnant on the common-adapter side, as these are trimmed from the 5' of the Reverse read.

        [CutadaptPatternTrimmer]
        barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT
        # barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT : TruSeq and Nextera Illumina barcode adapter on the i5 side (5' - 3').
        common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
        # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
        # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        common_side_cutsite_remnant = CTAA
        minimum_length = 60
        error_rate = 0
        output_directory = ./03_pattern_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # Pattern trimming searches for the reverse complement sequence of the common adapter fused to the restriction enzyme cutside remnant and trims these from the 3' of the Forward read.
        # Pattern trimming searches for the reverse complement sequence of the barcode adapter fused to the sample-specific barcode and the restriction enzyme cutside remnant and trims these from the 3' of the Reverse read.
        # in single-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is the same as on the barcode adapter side.

        [Pear]
        minimum_length = 60
        minimum_overlap = 10
        output_directory = ./04_merging
        output_file_name_template = {sample_name}_{orientation}_3.assembled{extension}
        # merges Forward and Reverse reads by sequence overlap.

        [MaxNFilter]
        max_n = 0 
        output_directory = ./05_max_n_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with N base calls.

        [SlidingWindowQualityFilter]
        window_size = 2
        average_quality = 20
        count = 1
        output_directory = ./06_sliding_window
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with low quality base calls.

        [AverageQualityFilter]
        average_quality = 25
        output_directory = ./07_average_quality_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes low quality reads.

        [RemovePatternFilter]
        pattern = CTGCAG
        # first enzyme
        output_directory = ./08_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

        [RemovePatternFilter]
        pattern = TTAA
        # second enzyme
        output_directory = ./09_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

Single-digest GBS, paired-end sequencing, merged reads
------------------------------------------------------


.. tabs::
   .. tab:: Scheme

      .. image:: images/merged_PE_SD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

            [General]
            cores = 32
            input_directory = /home/User/GBS_preprocessing
            sequencing_type = pe
            input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
            # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
            # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
            # {:XX} mark the number of characters in the file name that contain the specified information.
            # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps.

            [CutadaptDemultiplex]
            barcodes = /home/User/GBS_preprocessing/barcodes.fasta
            error_rate = 0
            output_directory = ./01_demultiplex
            output_file_name_template = {sample_name}_{orientation}{extension}
            barcode_side_cutsite_remnant = TGCAG
            # the (barcode + barcode_side_cutsite_remnant) sequence is searched per Forward read, to increase specificity of demultiplexing.
            # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

            [CutadaptPositionalTrimmer]
            barcodes = /home/User/GBS_preprocessing/barcodes.fasta
            barcode_side_cutsite_remnant = TGCAG
            common_side_cutsite_remnant = CTAA
            output_directory = ./02_positional_trimming
            output_file_name_template = {sample_name}_{orientation}{extension}
            # positional trimming needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.
            # positional trimming needs to know the length of the restriction enzyme cutside remnant on the common-adapter side, as these are trimmed from the 5' of the Reverse read.

            [CutadaptPatternTrimmer]
            barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT
            # barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT : TruSeq and Nextera Illumina barcode adapter on the i5 side (5' - 3').
            common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
            # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
            # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
            barcodes = /home/User/GBS_preprocessing/barcodes.fasta
            barcode_side_cutsite_remnant = TGCAG
            common_side_cutsite_remnant = CTAA
            minimum_length = 60
            error_rate = 0
            output_directory = ./03_pattern_trimming
            output_file_name_template = {sample_name}_{orientation}{extension}
            # Pattern trimming searches for the reverse complement sequence of the common adapter fused to the restriction enzyme cutside remnant and trims these from the 3' of the Forward read.
            # Pattern trimming searches for the reverse complement sequence of the barcode adapter fused to the sample-specific barcode and the restriction enzyme cutside remnant and trims these from the 3' of the Reverse read.
            # in single-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is the same as on the barcode adapter side.

            [Pear]
            minimum_length = 60
            minimum_overlap = 10
            output_directory = ./04_merging
            output_file_name_template = {sample_name}_{orientation}_3.assembled{extension}
            # merges Forward and Reverse reads by sequence overlap.

            [MaxNFilter]
            max_n = 0 
            output_directory = ./05_max_n_filter
            output_file_name_template = {sample_name}_{run}_3{extension}
            # removes reads with N base calls.

            [SlidingWindowQualityFilter]
            window_size = 2
            average_quality = 20
            count = 1
            output_directory = ./06_sliding_window
            output_file_name_template = {sample_name}_{run}_3{extension}
            # removes reads with low quality base calls.

            [AverageQualityFilter]
            average_quality = 25
            output_directory = ./07_average_quality_filter
            output_file_name_template = {sample_name}_{run}_3{extension}
            # removes low quality reads.

            [RemovePatternFilter]
            pattern = CTGCAG
            # first enzyme
            output_directory = ./08_remove_chimera_partial_digest
            output_file_name_template = {sample_name}_{run}_3{extension}
            # removes reads with intact internal restriction enzyme recognition sites.

            [RemovePatternFilter]
            pattern = TTAA
            # second enzyme
            output_directory = ./09_remove_chimera_partial_digest
            output_file_name_template = {sample_name}_{run}_3{extension}
            # removes reads with intact internal restriction enzyme recognition sites.


Double-digest GBS and single-end sequencing
-------------------------------------------

.. tabs::
   .. tab:: Scheme

      .. image:: images/separately_SE_DD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

        [General]
        cores = 32
        input_directory = /home/User/GBS_preprocessing
        sequencing_type = se
        input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
        # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
        # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
        # {:XX} mark the number of characters in the file name that contain the specified information.
        # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps.

        [CutadaptDemultiplex]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        error_rate = 0
        output_directory = ./01_demultiplex
        output_file_name_template = {sample_name}_{orientation}{extension}
        barcode_side_cutsite_remnant = TGCAG
        # the (barcode + barcode_side_cutsite_remnant) sequence is searched per read, to increase specificity of demultiplexing.
        # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

        [CutadaptPositionalTrimmer]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        output_directory = ./02_positional_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # positional trimming only needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.

        [CutadaptPatternTrimmer]
        common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
        # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
        # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
        common_side_cutsite_remnant = CTAA
        minimum_length = 60
        error_rate = 0
        output_directory = ./03_pattern_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # Pattern trimming searches for the reverse complement sequence of the common adapter fused to the restriction enzyme cutside remnant and trims these from the 3' of the Forward read.
        # in double-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is different from the barcode adapter side.

        [MaxNFilter]
        max_n = 0 
        output_directory = ./04_max_n_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with N base calls.

        [SlidingWindowQualityFilter]
        window_size = 2
        average_quality = 20
        count = 1
        output_directory = ./05_sliding_window
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with low quality base calls.


        [AverageQualityFilter]
        average_quality = 25
        output_directory = ./06_average_quality_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes low quality reads.

        [RemovePatternFilter]
        pattern = CTGCAG
        # first enzyme
        output_directory = ./07_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

        [RemovePatternFilter]
        pattern = TTAA
        # second enzyme
        output_directory = ./08_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

Double-digest GBS and paired-end sequencing, mapped separately
--------------------------------------------------------------
.. tabs::
   .. tab:: Scheme

      .. image:: images/separately_PE_DD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

        [General]
        cores = 32
        input_directory = /home/User/GBS_preprocessing
        sequencing_type = se
        input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
        # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
        # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
        # {:XX} mark the number of characters in the file name that contain the specified information.
        # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps.

        [CutadaptDemultiplex]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        error_rate = 0
        output_directory = ./01_demultiplex
        output_file_name_template = {sample_name}_{orientation}{extension}
        barcode_side_cutsite_remnant = TGCAG
        # the (barcode + barcode_side_cutsite_remnant) sequence is searched per read, to increase specificity of demultiplexing.
        # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

        [CutadaptPositionalTrimmer]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        output_directory = ./02_positional_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # positional trimming only needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.

        [CutadaptPatternTrimmer]
        common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
        # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
        # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
        common_side_cutsite_remnant = CTAA
        minimum_length = 60
        error_rate = 0
        output_directory = ./03_pattern_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # Pattern trimming searches for the reverse complement sequence of the common adapter fused to the restriction enzyme cutside remnant and trims these from the 3' of the Forward read.
        # in double-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is different from the barcode adapter side.

        [MaxNFilter]
        max_n = 0 
        output_directory = ./04_max_n_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with N base calls.

        [SlidingWindowQualityFilter]
        window_size = 2
        average_quality = 20
        count = 1
        output_directory = ./05_sliding_window
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with low quality base calls.


        [AverageQualityFilter]
        average_quality = 25
        output_directory = ./06_average_quality_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes low quality reads.

        [RemovePatternFilter]
        pattern = CTGCAG
        # first enzyme
        output_directory = ./07_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

        [RemovePatternFilter]
        pattern = TTAA
        # second enzyme
        output_directory = ./08_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

Double-digest GBS, paired-end sequencing, merged reads
------------------------------------------------------
.. tabs::
   .. tab:: Scheme

      .. image:: images/merged_PE_DD-GBS_SMAP_legend.png

   .. tab:: Config File

      .. code-block:: ini

        [General]
        cores = 32
        input_directory = /home/User/GBS_preprocessing
        sequencing_type = pe
        input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
        # Example: 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2 => run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
        # Essential information on run, read orientation, and file extension is obtained from the structure of the name of the original fastq file as provided by the service provider. 
        # {:XX} mark the number of characters in the file name that contain the specified information.
        # Fields "orientation" and "extension" are automatically transferred to all new file names created in the next steps.

        [CutadaptDemultiplex]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        error_rate = 0
        output_directory = ./01_demultiplex
        output_file_name_template = {sample_name}_{orientation}{extension}
        barcode_side_cutsite_remnant = TGCAG
        # the (barcode + barcode_side_cutsite_remnant) sequence is searched per Forward read, to increase specificity of demultiplexing.
        # Error rate is expressed as fraction of the searched length (range 0.0 - 1.0). 0 means exact match is required.

        [CutadaptPositionalTrimmer]
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        common_side_cutsite_remnant = CTAA
        output_directory = ./02_positional_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # positional trimming needs to know the length of the restriction enzyme cutside remnant on the barcode-adapter side, as these are trimmed from the 5' of the Forward read.
        # positional trimming needs to know the length of the restriction enzyme cutside remnant on the common-adapter side, as these are trimmed from the 5' of the Reverse read.

        [CutadaptPatternTrimmer]
        barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT
        # barcode_adapter_sequence = ACACTCTTTCCCTACACGACGCTCTTCCGATCT : TruSeq and Nextera Illumina barcode adapter on the i5 side (5' - 3').
        common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
        # common_adapter_sequence = GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT : TruSeq Illumina common adapter on the i7 side (5' - 3').
        # common_adapter_sequence = CGGTCTCGGCATTCCTGCTGAACCGCTCTTCCGATCT : Nextera Illumina common adapter on the i7 side (5' - 3').
        barcodes = /home/User/GBS_preprocessing/barcodes.fasta
        barcode_side_cutsite_remnant = TGCAG
        common_side_cutsite_remnant = CTAA
        minimum_length = 60
        error_rate = 0
        output_directory = ./03_pattern_trimming
        output_file_name_template = {sample_name}_{orientation}{extension}
        # Pattern trimming searches for the reverse complement sequence of the common adapter fused to the restriction enzyme cutside remnant and trims these from the 3' of the Forward read.
        # Pattern trimming searches for the reverse complement sequence of the barcode adapter fused to the sample-specific barcode and the restriction enzyme cutside remnant and trims these from the 3' of the Reverse read.
        # in double-digest GBS, the restriction enzyme cutsite remnant at the common adapter side is different from the barcode adapter side.

        [Pear]
        minimum_length = 60
        minimum_overlap = 10
        output_directory = ./04_merging
        output_file_name_template = {sample_name}_{orientation}_3.assembled{extension}
        # merges Forward and Reverse reads by sequence overlap.

        [MaxNFilter]
        max_n = 0 
        output_directory = ./05_max_n_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with N base calls.

        [SlidingWindowQualityFilter]
        window_size = 2
        average_quality = 20
        count = 1
        output_directory = ./06_sliding_window
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with low quality base calls.

        [AverageQualityFilter]
        average_quality = 25
        output_directory = ./07_average_quality_filter
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes low quality reads.

        [RemovePatternFilter]
        pattern = CTGCAG
        # first enzyme
        output_directory = ./08_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.

        [RemovePatternFilter]
        pattern = TTAA
        # second enzyme
        output_directory = ./09_remove_chimera_partial_digest
        output_file_name_template = {sample_name}_{run}_3{extension}
        # removes reads with intact internal restriction enzyme recognition sites.


Why merge?
----------

Paired-end reads
~~~~~~~~~~~~~~~~
   .. image:: images/paired_end_legend.png

Paired-end GBS reads are derived from two sides of an individual molecule. 
If NGS read length is longer than half of the PCR-amplified GBS fragment length 
(distance between two neighboring restriction enzyme cutsites (RE)), then those reads 
overlap at least partially in the middle of the GBS fragment. If reads are not merged prior to read mapping, 
it is best to run SMAP as if the reads were derived from single-end read data.
SMAP will treat these reads separately for the Definition of Stacks, StackClusters and MergedClusters 
and read-backed haplotyping using SMAP-haplotype.

In double-digest GBS, sequencing typically leads to exclusive mapping in one orientation, 
i.e. on one of both strands. Because most paired-end reads will overlap in the middle region 
of the GBS PCR-fragment, there is positional overlap between + strand mapped reads and - strand 
mapped reads, but the Forward and Reverse reads themselves do not span the entire GBS-fragment.

Separately mapped paired-end GBS reads have the advantage (compared to merged reads) of showing more
features causing polymorphic Stack Mapping Anchor Points (SMAPs, see #Polymorphisms affect the shape of Stacks).
Separately mapped paired-end GBS reads have the disadvantage (compared to merged reads) of shorter haplotype 
length, and that per locus, two partially overlapping Stacks are created by SMAP that carry partially redundant 
genetic information (via SNPs located in the overlap region), thus potentially artificially inflating the number 
of molecular markers.

Merged reads
~~~~~~~~~~~~
   .. image:: images/merged_legend.png

Paired-end GBS reads are derived from two sides of an individual molecule.
If NGS read length is longer than half of the PCR-amplified GBS fragment length 
(distance between two neighboring restriction enzyme cutsites (RE)), then those reads overlap 
at least partially in the middle of the GBS fragment.
Merging paired-end read data (e.g. by PEAR) generates a sequence that spans the entire length of 
the GBS-fragment (between two neighboring RE’s). Merged GBS reads have the *advantage* 
(compared to separately mapped paired-end reads) that SMAP does not need to consider strandedness 
of mapping. This leads to a single non-redundant observation of allele frequency per locus, longer haplotypes, 
and with the maximum read depth across the length of the fragment. Merged GBS reads have the *disadvantage* 
(compared to separately mapped paired-end reads), that Stacks derived from merged reads show fewer features
causing polymorphic Stack Mapping Anchor Points (SMAPs, see SMAPs in separately mapped reads versus merged reads).

