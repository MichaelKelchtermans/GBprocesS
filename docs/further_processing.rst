Mapping and further processing
==============================

We recommend using `BWA-mem <http://bio-bwa.sourceforge.net/bwa.shtml>`_ to map short GBS reads to a reference sequence and to analyze 
the read depth distribution with SMAP-delineate.

