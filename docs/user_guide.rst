==========
User Guide
==========

GBprocesS allows for the extraction of genomic inserts from NGS data for GBS experiments.
Preprocessing is performed in different stages that are part of a linear pipeline 
where the steps are performed in order. GBprocesS provides a flexible way to adjust 
the functionality to your needs, as the operations required and the execution order
vary depending on the GBS protocol used.

Operations
----------
Provided in this software are a number of predefined operations that may be put 
together to form a preprocessing pipeling. A list of operations is available through::

    gbprocess --operations

Which provides a list of the names of all available operations. For a detailed explanation
on what specific operation does and how to configure it, please refer to the Operations_ section.

Configuring GBprocesS
---------------------
The configuration syntax used by GBprocesS follows the `INI-file <https://en.wikipedia.org/wiki/INI_file>`_ format.
This format defines sections, parameters and comments. Note that sections and parameter definitions
are case sensitive.

Sections start with ``[`` and end with ``]``, and the plain text between the brackets is the 
section title (e.g. ``[Title1]``). GBprocesS will parse sections in order, starting at the top
of the configuration file. GBprocesS recognizes two types of sections: the ``[General]`` section
and sections that define an operation to be executed. Below each section header, paremetes can 
be defined. Parameters are defined by using the syntax ``parameter_name = parameter_value``:
the name of the parameter and the parameter value itself, separated by an equal sign ``=``. 

General
~~~~~~~

The first section that needs to be specified is the ``[General]`` section. The general section
allows to configure pipeline behaviour, independant of the operations that will be performed
by the pipeline:

    - ``cores`` (int): Defines the maximum number of CPU cores to be used by GBprocesS.
    - ``input_directory`` (str): Path to an existing directory that contains the .fastq files to be processed.
    - ``sequencing_type`` (str): either ``pe`` or ``se`` indicating if the .fastq files in 
      ``input_directory`` are paired end sequences or single end and merged reads respectively.
    - ``input_file_name_template`` (str): Template that will be used to interprete the names of the input .fastq files. 
      The template follows a simplified syntax of format strings as 
      descibed in `PEP 3101 <https://www.python.org/dev/peps/pep-3101/#id17/>`__. The template 
      consists of text data that describes parts of the file names that are not captured
      and replacement fields (indicated by curly braces) that describe parts of the filenames that are 
      stored as a property of the sample. These properties can be used later to determine the format 
      of the output files (see ``output_file_name_template`` in Operations_ section). The field name,
      the element inside the curly braces of the replacement field, refers to the property name. Possible properties are:
      ``{orientation}``, ``{run}``, ``{sample_name}`` and ``{extension}``. The field name can be followed by a colon (``:``) 
      and a number which indicates the with of the field in the input file name. 
      This means that the part of this file name is of equal length in all files in the input directory.
      Please note that if two adjacent fields do not have a specified width, it will not be possible to parse them,
      as it is impossible to assign a part of the input file name to one field or the other. 
      Thus, only one field can not have width specified in the template, and for this field the maximum width possible is used.
    - ``temp_dir`` (str, optional): path to a folder to store the temporary files into. As a rule of thumb, this temporary directory 
      must be able to hold approximatly two times the amount of data present in ``input_directory``, 
      defaults to ``/tmp/``.

Example::

    [General]
    # Use 1 CPU core
    cores = 1
    # Location of the input files
    input_directory = /data/run/
    # Paired end sequencing
    sequencing_type = pe
    # Template to parse the input files.
    ## For example, 17146FL-13-01-01_S9_L002_R1_001.fastq.bz2: run = 17146FL-13-01-01_S9_L002; orientation = 1; extension = .fastq.bz2
    input_file_name_template = {run:24}_R{orientation:1}_001{extension:10}
    # Location to store temporary created files
    temp_dir = /tmp/

Configuring Operations
~~~~~~~~~~~~~~~~~~~~~~

Any section following the general section will be interpreted as being an operation to be added to the pipeline.


Starting the pipeline
---------------------


Debugging
---------
By default, only run information is reported when executing a GBprocesS.
This means that no stack trace is provided on error.
To enable the reporting of debugging information, the ``--debug`` flag can be used::

    gbprocess --debug -c example_configuration.ini