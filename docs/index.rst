=========
GBprocesS
=========


.. toctree::
   :maxdepth: 2

   installation
   user_guide
   operations
   gbs_data_processing
   gbs_data_example
   further_processing
