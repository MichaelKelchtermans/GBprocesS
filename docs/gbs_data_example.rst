GBS data generation and processing example
==========================================

Single-digest GBS library preparation and paired-end sequencing
---------------------------------------------------------------

.. tabs::
   .. tab:: Restriction Digest

      .. image:: images/example/A_Restriction_digest.png

   .. tab:: Ligation

        .. image:: images/example/B_ligation.png
    
   .. tab:: PCR-amplification

        .. image:: images/example/C_PCR_amplification.png
          

   .. tab:: Sequencing forward reads

        Forward read 75 bp  
        
        .. image:: images/example/D_sequencing_Forward_75bp.png

        Forward read 100 bp  
        
        .. image:: images/example/D_sequencing_Forward_100bp.png

   .. tab:: Sequencing reverse reads
        
        Reverse read 75 bp
        
        .. image:: images/example/D_sequencing_Reverse_75bp.png

        Reverse read 100 bp
        
        .. image:: images/example/D_sequencing_Reverse_100bp.png


Demultiplexing
--------------

.. image:: images/example/E_Demultiplexing_Forward_100bp.png

Positional Trimming
-------------------

.. tabs::
    .. tab:: Forward read
        
        Forward read 75 bp

        .. image:: images/example/E_Correct_positional_trimming_Forward_75bp.png

        Forward read 100bp

        .. image:: images/example/E_Correct_positional_trimming_Forward_100bp.png

    .. tab:: Reverse read

        Reverse read 75 bp

        .. image:: images/example/E_Correct_positional_trimming_Reverse_75bp.png

        Reverse read 100 bp

        .. image:: images/example/E_Correct_positional_trimming_Reverse_100bp.png


Pattern Trimming
----------------

.. tabs::
    .. tab:: Forward read
        
        Forward read 75 bp

        .. image:: images/example/H_Pattern_trimming_Forward_75bp.png

        Forward read 100bp

        .. image:: images/example/H_Pattern_trimming_Forward_100bp.png

    .. tab:: Reverse read

        Reverse read 75 bp

        .. image:: images/example/H_Pattern_trimming_Reverse_75bp.png

        Reverse read 100 bp

        .. image:: images/example/H_Pattern_trimming_Reverse_100bp.png

Merging
-------

.. tabs::
    .. tab:: Merge paired-end 75bp
        
        Forward read 75 bp

        .. image:: images/example/I_merging_75bp.png

        
    .. tab:: Merge paired-end 100bp

        Reverse read 75 bp

        .. image:: images/example/I_merging_100bp.png


