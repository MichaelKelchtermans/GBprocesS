from setuptools import find_packages
from distutils.core import setup
setup(name='gbprocess',
      use_scm_version={'write_to': 'gbprocess/version.py'},
      setup_requires=['setuptools_scm', 'setuptools_scm_git_archive'],
      extras_require={'docs': ['sphinx','sphinx_rtd_theme','sphinx-tabs']},
      description='A pipeline builder for GBS data.',
      author='Dries Schaumont',
      author_email='dries.schaumont@ilvo.vlaanderen.be',
      install_requires=['BioPython', 'cutadapt'],
      entry_points={
        'console_scripts': [
            'gbprocess = gbprocess.__main__:main',
            ],
        },
        packages = ['gbprocess','gbprocess.operations'],
      python_requires='>=3.6',
)
