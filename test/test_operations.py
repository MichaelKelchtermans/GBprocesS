import sys
import time
import unittest
from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest.mock import MagicMock, patch
from textwrap import dedent

from gbprocess.data import SequencingData, Fastq, PairedEndFastq, SingleEndFastq
from gbprocess.operations.demultiplexing import CutadaptDemultiplex
from gbprocess.operations.filtering import (AverageQualityFilter, LengthFilter,
                                      MaxNFilter, SlidingWindowQualityFilter)
from gbprocess.operations.merging import FastqJoinMerger
from gbprocess.pipeline import SerialPipeline
from gbprocess.operations.trimming import CutadaptPatternTrimmer

from data import barcodes, fastq_forward, fastq_reverse, fastq_forward_no_at, fastq_reverse_no_at
from utils import file_contents_equals

class CustomTestCase(unittest.TestCase):
    def setUp(self):
        self.tempdir = TemporaryDirectory()
        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, open(self.tempdir.name + "/barcodes.fasta", 'wb+') as barcodes_file:
            temp_file_forward.write(fastq_forward())
            temp_file_reverse.write(fastq_reverse())
            barcodes_file.write(barcodes())
            temp_file_forward.flush()
            temp_file_reverse.flush()
            barcodes_file.flush()
            self.barcodes = self.tempdir.name + "/barcodes.fasta"
            self.p_fastq = PairedEndFastq(temp_file_forward.name, temp_file_reverse.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)
            self.s_fastq = SingleEndFastq(temp_file_forward.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)
            self.p_seq_data = SequencingData([self.p_fastq])
            self.s_seq_data = SequencingData([self.s_fastq])
            self.pipeline = SerialPipeline()  

    def tearDown(self):
        self.tempdir.cleanup()

class TestDemultiplexing(CustomTestCase):
    def setUp(self):
        self.barcode_1_f = dedent("""\
                                  @EU861894-140/1
                                  CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                  +
                                  ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
                                  """).strip().encode()
        self.barcode_2_f = dedent("""\
                                  @EU861894-138/1
                                  AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                  +
                                  @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
                                  """).strip().encode()
        self.barcode_1_r = dedent("""\
                                  @EU861894-140/2
                                  AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                  +
                                  @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                  
                                  """).strip().encode()
        self.barcode_2_r = dedent("""\
                                  @EU861894-138/2
                                  CTTCGGGGGTGGTTAGGCAACCCCCCCCGAAGCGGGGGACAACAGCCTTAAACGGTTCCTAATACCGCATGGTGA
                                  +
                                  BB@FB2@FHB2HFJGFFHJ?8=##JDGHDEIBH?H#HI)EFFEF#C#B#HE?#D?#;#DDCA#:DD>BCB###D'
                                  
                                  """).strip().encode()
        
        super().setUp()

    def test_demultiplexing_paired(self):
        demultiplexer = CutadaptDemultiplex(error_rate=0.0, barcodes=self.barcodes, output_file_name_template="{sample_name}_{run}_{orientation}{extension}", output_directory=self.tempdir.name)
        self.pipeline.add_operation(demultiplexer)
        self.pipeline.execute(self.p_seq_data)
        forward_1, reverse_1 = Path(self.tempdir.name + "/barcode1_17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/barcode1_17146FL-13-01-01_S97_2.fastq")
        forward_2, reverse_2 = Path(self.tempdir.name + "/barcode2_17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/barcode2_17146FL-13-01-01_S97_2.fastq")
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        self.assertIn(forward_1, files)
        self.assertIn(reverse_1, files)
        self.assertIn(forward_2, files)
        self.assertIn(reverse_2, files)
        self.assertTrue(file_contents_equals(forward_1, self.barcode_1_f))
        self.assertTrue(file_contents_equals(forward_2, self.barcode_2_f))
        self.assertTrue(file_contents_equals(reverse_1, self.barcode_1_r))
        self.assertTrue(file_contents_equals(reverse_2, self.barcode_2_r))

    def test_demultiplexing_single(self):
        #This test failes when debugging, the files do not exist
        demultiplexer = CutadaptDemultiplex(error_rate=0.0, barcodes=self.barcodes, output_file_name_template="{sample_name}_{run}{extension}", output_directory=self.tempdir.name)
        self.pipeline.add_operation(demultiplexer)
        self.pipeline.execute(self.s_seq_data)

        files = list(Path(self.tempdir.name).glob("*.fastq"))
        forward_1, forward_2 = Path(self.tempdir.name + "/barcode1_17146FL-13-01-01_S97.fastq"), Path(self.tempdir.name + "/barcode2_17146FL-13-01-01_S97.fastq")
        self.assertIn(forward_1, files)
        self.assertIn(forward_2, files)
        self.assertTrue(file_contents_equals(forward_1, self.barcode_1_f))
        self.assertTrue(file_contents_equals(forward_2, self.barcode_2_f))

    def test_demultiplexing_error_rate_boundry_raises(self):
        with self.assertRaises(AssertionError):
            CutadaptDemultiplex(error_rate=2, barcodes=self.barcodes, output_file_name_template="{sample_name}_{run}_{orientation}{extension}", output_directory=self.tempdir.name)
        
    def test_demultiplexing_error_rate_not_float_raises(self):
        with self.assertRaises(ValueError):
            CutadaptDemultiplex(error_rate="a", barcodes=self.barcodes, output_file_name_template="{sample_name}_{run}_{orientation}{extension}", output_directory=self.tempdir.name)
    
class TestMerging(CustomTestCase):
    def setUp(self):
        self.tempdir = TemporaryDirectory()
        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse:
            temp_file_forward.write(fastq_forward_no_at())
            temp_file_reverse.write(fastq_reverse_no_at())
            temp_file_forward.flush()
            temp_file_reverse.flush()
            self.p_fastq = PairedEndFastq(temp_file_forward.name, temp_file_reverse.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)
            self.s_fastq = SingleEndFastq(temp_file_forward.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)
            self.p_seq_data = SequencingData([self.p_fastq])
            self.s_seq_data = SequencingData([self.s_fastq])
            self.pipeline = SerialPipeline()
            self.merged = dedent("""\
                                @EU861894-140/1
                                CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTG\
                                    TACCTGCTCCACCGCTTGCTCCCGCTCTCCGTGGGTTTCAAAGCCTGGTGAGGTTCTTCAGTTA\
                                        GTGACGAATTAATGCACATACCGCACCGCTTATGCGTGCCCCTGTCAATTCCTT
                                +
                                ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C\
                                =)I@#BHBBCDCD;ACDCD;?ECF>?D<:DAC#C#BFEDC##4DC?E#DI=6H<II)#IGIJDGG>JBIF\
                                BEGJJCIIJB)#B##IJG?J:434IDHE!BJGGA#AFFEFF1F@1F
                                """).strip().encode()

    def test_merging_paired(self):
        merger = FastqJoinMerger(minimum_overlap = 1, percent_maximum_difference = 1, output_directory = self.tempdir.name, output_file_name_template="{run}.join{extension}")
        self.pipeline.add_operation(merger)
        self.pipeline.execute(self.p_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        output_file = Path(self.tempdir.name + "/17146FL-13-01-0.join.fastq")
        self.assertIn(output_file, files)
        self.assertFalse(file_contents_equals(output_file, self.merged))

    def test_merging_single_raises(self):
        merger = FastqJoinMerger(minimum_overlap = 1, percent_maximum_difference = 1, output_directory = self.tempdir.name, output_file_name_template="{run}.join{extension}")
        self.pipeline.add_operation(merger)
        with self.assertRaises(ValueError):
            self.pipeline.execute(self.s_seq_data)

class TestDiscardMaxN(CustomTestCase):
    def setUp(self):
        self.forward_out = dedent(
                                """
                                @EU861894-138/1
                                AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                +
                                @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                """).strip().encode()
        self.reverse_out = dedent(
                                """
                                @EU861894-138/2
                                CTTCGGGGGTGGTTAGGCAACCCCCCCCGAAGCGGGGGACAACAGCCTTAAACGGTTCCTAATACCGCATGGTGA
                                +
                                BB@FB2@FHB2HFJGFFHJ?8=##JDGHDEIBH?H#HI)EFFEF#C#B#HE?#D?#;#DDCA#:DD>BCB###D'
                                """).strip().encode()
        
        super().setUp()

    def test_filter_max_paired(self):
        filter_operation = MaxNFilter(max_n=0, output_directory=self.tempdir.name, output_file_name_template="{run}_{orientation}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.p_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward, reverse = Path(self.tempdir.name + "/17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_2.fastq")
        self.assertIn(foward, files)
        self.assertIn(reverse, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))
        self.assertTrue(file_contents_equals(reverse, self.reverse_out))

    def test_filter_max_single(self):
        filter_operation = MaxNFilter(max_n=0, output_directory=self.tempdir.name, output_file_name_template="{run}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.s_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward = Path(self.tempdir.name + "/17146FL-13-01-01_S97.fastq")
        self.assertIn(foward, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))

class TestLengthFilter(CustomTestCase):
    def setUp(self):
        self.forward_out = dedent(
                                """
                                @EU861894-140/1
                                CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                +
                                ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                """).strip().encode()
        self.reverse_out = dedent(
                                """
                                @EU861894-140/2
                                AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                +
                                @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                """).strip().encode()
        super().setUp()

    def test_filter_length_paired(self):
        filter_operation = LengthFilter(minimum_length=100, output_directory=self.tempdir.name, output_file_name_template="{run}_{orientation}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.p_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward, reverse = Path(self.tempdir.name + "/17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_2.fastq")
        self.assertIn(foward, files)
        self.assertIn(reverse, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))
        self.assertTrue(file_contents_equals(reverse, self.reverse_out))

    def test_filter_length_single(self):
        filter_operation = LengthFilter(minimum_length=100, output_directory=self.tempdir.name, output_file_name_template="{run}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.s_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward = Path(self.tempdir.name + "/17146FL-13-01-01_S97.fastq")
        self.assertIn(foward, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))

class TestAverageQualityFilter(CustomTestCase):
    def setUp(self):
        self.forward_out = dedent(
                                """
                                @EU861894-140/1
                                CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                +
                                ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                """).strip().encode()
        self.reverse_out = dedent(
                                """
                                @EU861894-140/2
                                AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                +
                                @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                """).strip().encode()
        self.single_out = dedent(
                                """
                                @EU861894-138/1
                                AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                +
                                @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                """).strip().encode()
        super().setUp()

    def test_average_quality_filter_paired(self):
        filter_operation = AverageQualityFilter(average_quality=28, output_directory=self.tempdir.name, output_file_name_template="{run}_{orientation}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.p_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward, reverse = Path(self.tempdir.name + "/17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_2.fastq")
        self.assertIn(foward, files)
        self.assertIn(reverse, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))
        self.assertTrue(file_contents_equals(reverse, self.reverse_out))

    def test_average_quality_filter_single(self):
        filter_operation = AverageQualityFilter(average_quality=31, output_directory=self.tempdir.name, output_file_name_template="{run}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.s_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward = Path(self.tempdir.name + "/17146FL-13-01-01_S97.fastq")
        self.assertIn(foward, files)
        self.assertTrue(file_contents_equals(foward, self.single_out))

class TestSlidingWindowQualityFilter(CustomTestCase):
    def setUp(self):
        self.forward_out = dedent(
                                """
                                @EU861894-140/1
                                CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                +
                                ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                """).strip().encode()
        self.reverse_out = dedent(
                                """
                                @EU861894-140/2
                                AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                +
                                @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                """).strip().encode()
        super().setUp()
    def test_sliding_window_filter_paired(self):
        filter_operation = SlidingWindowQualityFilter(window_size=5, average_quality=28, count=2, output_directory=self.tempdir.name, output_file_name_template="{run}_{orientation}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.p_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward, reverse = Path(self.tempdir.name + "/17146FL-13-01-01_S97_1.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_2.fastq")
        self.assertIn(foward, files)
        self.assertIn(reverse, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))
        self.assertTrue(file_contents_equals(reverse, self.reverse_out))

    def test_sliding_window_filter_single(self):
        filter_operation = SlidingWindowQualityFilter(window_size=5, average_quality=28, count=1, output_directory=self.tempdir.name, output_file_name_template="{run}{extension}")
        self.pipeline.add_operation(filter_operation)
        self.pipeline.execute(self.s_seq_data)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        foward = Path(self.tempdir.name + "/17146FL-13-01-01_S97.fastq")
        self.assertIn(foward, files)
        self.assertTrue(file_contents_equals(foward, self.forward_out))

#         
# class TestJoining(CustomTestCase):
#     def setUp(self):
#         self.read2_f = dedent("""\
#                                   @EU861894-140/1
#                                   CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
#                                   +
#                                   ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
#                                   """).strip().encode()
#         self.read1_f = dedent("""\
#                                   @EU861894-138/1
#                                   AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
#                                   +
#                                   @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
#                                   """).strip().encode()
#         self.read2_r = dedent("""\
#                                   @EU861894-140/2
#                                   AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
#                                   +
#                                   @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                  
#                                   """).strip().encode()
#         self.read1_r = dedent("""\
#                                   @EU861894-138/2
#                                   CTTCGGGGGTGGTTAGGCAACCCCCCCCGAAGCGGGGGACAACAGCCTTAAACGGTTCCTAATACCGCATGGTGA
#                                   +
#                                   BB@FB2@FHB2HFJGFFHJ?8=##JDGHDEIBH?H#HI)EFFEF#C#B#HE?#D?#;#DDCA#:DD>BCB###D'
                                  
#                                   """).strip().encode()
        
#         super().setUp()

#     def test_join_single(self):
#         with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq", 'wb+') as temp_file_forward_0,\
#              open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq", 'wb+') as temp_file_forward_1:
#             temp_file_forward_0.write(self.read1_f)
#             temp_file_forward_0.flush()
#             temp_file_forward_1.write(self.read2_f)
#             temp_file_forward_1.flush()

#         self.s_fastq_0 = SingleEndFastq(temp_file_forward_0.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)
#         self.s_fastq_1 = SingleEndFastq(temp_file_forward_1.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)

#         join_op = _Join("{run}_{extension}", self.tempdir.name)
#         join_op.perform(self.s_fastq_1)
#         result = join_op.perform(self.s_fastq_0)
#         fastq_result = list(result)
#         self.assertTrue(len(fastq_result) == 1) 
#         [foward_result] = fastq_result.pop().files
#         self.assertTrue(file_contents_equals(foward_result, fastq_forward()))

#     def test_join_paired(self):
#             with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq", 'wb+') as temp_file_forward_0,\
#                 open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_0.fastq", 'wb+') as temp_file_reverse_0,\
#                 open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq", 'wb+') as temp_file_forward_1,\
#                 open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_1.fastq", 'wb+') as temp_file_reverse_1:

#                 temp_file_forward_0.write(self.read1_f)
#                 temp_file_forward_0.flush()
#                 temp_file_forward_1.write(self.read2_f)
#                 temp_file_forward_1.flush()
#                 temp_file_reverse_0.write(self.read1_r)
#                 temp_file_reverse_0.flush()
#                 temp_file_reverse_1.write(self.read2_r)
#                 temp_file_reverse_1.flush()

#             self.p_fastq_0 = PairedEndFastq(temp_file_forward_0.name, temp_file_reverse_0.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)
#             self.p_fastq_1 = PairedEndFastq(temp_file_forward_1.name, temp_file_reverse_1.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)

#             join_op = _Join("{run}_{orientation}{extension}", self.tempdir.name)
#             join_op.perform(self.p_fastq_1)
#             result = join_op.perform(self.p_fastq_0)
#             fastq_result = list(result)
#             self.assertTrue(len(fastq_result) == 1) 
#             foward_result, reverse_result = fastq_result.pop().files
#             self.assertTrue(file_contents_equals(foward_result, fastq_forward()))
#             self.assertTrue(file_contents_equals(reverse_result, fastq_reverse()))


class TestCutadaptPatternTrimmer(CustomTestCase):
    def setUp(self):
        super().setUp()
        barcode = dedent(
            """
            >barcode1
            ATCCCCGGG
            """).strip().encode()
        with open(self.tempdir.name + "/barcodes.fasta", 'wb+') as barcodes_file:
            barcodes_file.write(barcode)
            barcodes_file.flush()
        self.barcodes = self.tempdir.name + "/barcodes.fasta"
        self.p_fastq.sample_name = "barcode1"
        self.s_fastq.sample_name = "barcode1"
        self.read1_r = dedent("""
                              @EU861894-140/1
                              CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACT
                              +
                              ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G
                              @EU861894-138/1
                              AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                              +
                              @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D   
                              """).strip().encode()

    def test_trim_single(self):
        trim_op = CutadaptPatternTrimmer(common_adapter_sequence="GTGGAGCAGGTAC", 
                                         barcode_adapter_sequence="", 
                                         common_side_cutsite_remnant="GCG", 
                                         barcode_side_cutsite_remnant="", 
                                         barcodes=self.barcodes, 
                                         minimum_length=1, 
                                         error_rate=0, 
                                         output_file_name_template="{run}_{extension}", 
                                         output_directory=self.tempdir.name)
        result = trim_op.perform(self.s_fastq)
        fastq_result = list(result)
        self.assertTrue(len(fastq_result) == 1) 
        fastq, = fastq_result.pop().files
        self.assertTrue(file_contents_equals(fastq,  self.read1_r))


class TestCutadaptPatternTrimmer(CustomTestCase):
    def setUp(self):
        super().setUp()
        barcode = dedent(
            """
            >barcode1
            ATCCCCGGG
            """).strip().encode()
        with open(self.tempdir.name + "/barcodes.fasta", 'wb+') as barcodes_file:
            barcodes_file.write(barcode)
            barcodes_file.flush()
        self.barcodes = self.tempdir.name + "/barcodes.fasta"
        self.p_fastq.sample_name = "barcode1"
        self.s_fastq.sample_name = "barcode1"
        self.read1_r = dedent("""
                              @EU861894-140/1
                              CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACT
                              +
                              ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G
                              @EU861894-138/1
                              AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                              +
                              @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D   
                              """).strip().encode()

    def test_trim_single(self):
        trim_op = CutadaptPatternTrimmer(common_adapter_sequence="GTGGAGCAGGTAC", 
                                         barcode_adapter_sequence="", 
                                         common_side_cutsite_remnant="GCG", 
                                         barcode_side_cutsite_remnant="", 
                                         barcodes=self.barcodes, 
                                         minimum_length=1, 
                                         error_rate=0, 
                                         output_file_name_template="{run}_{extension}", 
                                         output_directory=self.tempdir.name)
        result = trim_op.perform(self.s_fastq)
        fastq_result = list(result)
        self.assertTrue(len(fastq_result) == 1) 
        fastq, = fastq_result.pop().files
        self.assertTrue(file_contents_equals(fastq,  self.read1_r))

    def test_issue_2(self):
        with self.assertRaises(ValueError):
            trim_op = CutadaptPatternTrimmer(common_adapter_sequence="", 
                                            barcode_adapter_sequence="GTGGAGCAGGTAC", 
                                            common_side_cutsite_remnant="", 
                                            barcode_side_cutsite_remnant="GCG", 
                                            barcodes=self.barcodes, 
                                            minimum_length=1, 
                                            error_rate=0, 
                                            output_file_name_template="{run}_{extension}", 
                                            output_directory=self.tempdir.name)

    def test_issue_1(self):
        trim_op = CutadaptPatternTrimmer(common_adapter_sequence="GTGGAGCAGGTAC", 
                                         barcode_adapter_sequence="", 
                                         common_side_cutsite_remnant="GCG", 
                                         barcode_side_cutsite_remnant="", 
                                         barcodes=self.barcodes, 
                                         minimum_length=1, 
                                         error_rate=0.5, 
                                         output_file_name_template="{run}_{extension}", 
                                         output_directory=self.tempdir.name)