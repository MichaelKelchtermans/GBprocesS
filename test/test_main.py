import unittest
from gbprocess.__main__ import main
import sys
from tempfile import TemporaryDirectory
import logging
import io
from gbprocess.operations.operation import _ALL_OPERATIONS
from data import config_only_general_section, fastq_forward, fastq_reverse, config_duplicate
from importlib import reload
from unittest.mock import patch, NonCallableMock
from gbprocess.pipeline import SerialPipeline, MultiProcessPipeline
from configparser import DuplicateSectionError
from gbprocess.data import SequencingData, PairedEndFastq

class TestCommandLine(unittest.TestCase):
    def tearDown(self):
        logging.shutdown()
        reload(logging)

    def test_main_sys_args(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=sys.argv)
        self.assertNotEqual(cm.exception.code, 0)

    def test_main_sys_args_empty(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=sys.argv[1:])
        self.assertNotEqual(cm.exception.code, 0)

    def test_main_sys_args_none(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=None)
        self.assertNotEqual(cm.exception.code, 0)

    def test_main_help(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=['--help'])
        self.assertEqual(cm.exception.code, 0)

    def test_main_without_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertNotEqual(cm.exception.code, 0)

    def test_main_no_config(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=['--config'])
        self.assertNotEqual(cm.exception.code, 0)

    def test_default_logging_level(self):
        with self.assertRaises(SystemExit) as cm:
            main(args=['--help'])
        self.assertEqual(cm.exception.code, 0)
        logger = logging.getLogger("Test")
        # Note that the root logger is created with level WARNING.
        self.assertEqual(logger.getEffectiveLevel(), logging.WARNING) 

    def test_main_debug(self):
        with self.assertRaises(SystemExit):
            main(args=["--debug"])
        logger = logging.getLogger("Test")
        self.assertEqual(logger.getEffectiveLevel(), logging.DEBUG)

    def test_main_list_operations(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        main(args=["--operations"])
        sys.stdout = sys.__stdout__
        self.assertIn("\n".join(_ALL_OPERATIONS), capturedOutput.getvalue())

    @patch.object(SerialPipeline, 'add_operation')
    def test_main_empty_config(self, mocked_my_operation, new_callable=NonCallableMock):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                config.write(config_only_general_section(tempdir, "{run:25}_R{orientation:1}_001{extension}"))
                config.flush()
                capturedOutput = io.StringIO()
                sys.stdout = capturedOutput
                main(args=['--config', config.name])
                sys.stdout = sys.__stdout__

    @patch.object(SerialPipeline, 'execute')
    def test_main_duplicate_operations(self, mocked_my_operation, return_value=None):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                config.write(config_duplicate(tempdir, "{run:25}_R{orientation:1}_001{extension}"))
                config.flush()
                try:
                    main(args=['--config', config.name])
                except DuplicateSectionError:
                    self.fail("Duplicate sections should be allowed in the configuration.")


    @patch.object(SerialPipeline, 'add_operation')
    def test_wrong_sequencing_type(self, mocked_my_operation, new_callable=NonCallableMock):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                config.write(config_only_general_section(tempdir, "{run:25}_R{orientation:1}_001{extension}", sequencing_type='foo'))
                config.flush()
                with self.assertRaises(ValueError): 
                    main(args=['--config', config.name])

    def test_main_both_coverage_and_config(self):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                config.write(config_only_general_section(tempdir, "{run:25}_R{orientation:1}_001{extension}"))
                config.flush()
                capturedOutput = io.StringIO()
                with self.assertRaises(SystemExit) as cm:
                    sys.stdout = capturedOutput
                    main(args=['--operations', '--config', config.name])
                    sys.stdout = sys.__stdout__
                self.assertNotEqual(cm.exception.code, 0)
                self.assertIn("Both --config and --operations specified.", capturedOutput.getvalue())

    def test_config_not_a_file(self):
        with self.assertRaises(ValueError):
            main(args=['--config', "/tmp/foo"])

    def test_config_empty(self):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/config.ini", 'wt+') as config:
                config.write('\n')
                config.flush()
                with self.assertRaises(ValueError):
                    main(args=['--config', tempdir + "/config.ini"])
                    
    def test_issue3_multi_pipeline(self):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                fastq = PairedEndFastq(temp_file_forward.name, 
                                       temp_file_reverse.name, 
                                       run="17146FL-13-01-01_S97", 
                                       extension=".fastq", 
                                       orientation=[1,2], 
                                       exists_ok=True)
                seq_data = SequencingData([fastq])
                pipeline = MultiProcessPipeline(temp_dir=tempdir)
                pipeline.execute(seq_data)
    
    def test_issue3_serial_pipeline(self):
        with TemporaryDirectory() as tempdir:
            with open(tempdir + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
                 open(tempdir + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse, \
                 open(tempdir + "/config.ini", 'wb+') as config:
                temp_file_forward.write(fastq_forward())
                temp_file_reverse.write(fastq_reverse())
                temp_file_forward.flush()
                temp_file_reverse.flush()
                fastq = PairedEndFastq(temp_file_forward.name, 
                                       temp_file_reverse.name, 
                                       run="17146FL-13-01-01_S97", 
                                       extension=".fastq", 
                                       orientation=[1,2], 
                                       exists_ok=True)
                seq_data = SequencingData([fastq])
                pipeline = SerialPipeline(temp_dir=tempdir)
                pipeline.execute(seq_data)
                
            
