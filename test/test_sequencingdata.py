import bz2
import gzip
import lzma
import shutil
import unittest
from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from textwrap import dedent
from unittest.mock import MagicMock, patch

from gbprocess.data import SequencingData, Fastq, PairedEndFastq, SingleEndFastq, _FastqFileNameTemplate
from data import barcodes, fastq_forward, fastq_reverse
from utils import file_contents_equals


class TestPairedEndFastq(unittest.TestCase):
    def setUp(self):
        self.tempdir = TemporaryDirectory()
        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, \
            open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse:
            temp_file_forward.write(fastq_forward())
            temp_file_reverse.write(fastq_reverse())
            temp_file_forward.flush()
            temp_file_reverse.flush()
            self.forward = self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq"
            self.reverse = self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001.fastq"

        self.forward_gz = self.forward + ".gz"
        self.reverse_gz = self.reverse + ".gz"
        with open(self.forward, 'rb') as f_in:
            with gzip.open(self.forward_gz, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        
        with open(self.reverse, 'rb') as f_in:
            with gzip.open(self.reverse_gz, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        self.forward_bzip2 = self.forward + ".bzip2"
        self.reverse_bzip2 = self.reverse + ".bzip2"
        with open(self.forward, 'rb') as f_in:
            with bz2.open(self.forward_bzip2, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        
        with open(self.reverse, 'rb') as f_in:
            with bz2.open(self.reverse_bzip2, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        

    def tearDown(self):
        self.tempdir.cleanup()

    def test_create_uncompressed(self):
        fastq = PairedEndFastq(self.forward, self.reverse, exists_ok=True)
        self.assertListEqual(fastq.files, [Path(self.forward), Path(self.reverse)])        
        self.assertEqual(fastq.run, None)
        self.assertEqual(fastq.extension, None)
        self.assertEqual(fastq.sample_name, None)
        self.assertEqual(fastq.orientation, None)
        fastq = PairedEndFastq(self.forward, self.reverse, run="17146FL-13-01-01_S97", sample_name="", orientation=[1,2], extension=".fastq", exists_ok=True)
        self.assertEqual(fastq.run, "17146FL-13-01-01_S97")
        self.assertEqual(fastq.extension, ".fastq")
        self.assertEqual(fastq.sample_name, "")
        self.assertTupleEqual(fastq.orientation, (1,2))

    def test_create_compressed(self):       
        fastq = PairedEndFastq(self.forward_gz, self.reverse_gz, exists_ok=True)
        self.assertListEqual(fastq.files, [Path(self.forward_gz), Path(self.reverse_gz)])        
        self.assertEqual(fastq.run, None)
        self.assertEqual(fastq.extension, None)
        self.assertEqual(fastq.sample_name, None)
        self.assertEqual(fastq.orientation, None)

        
        fastq = PairedEndFastq(self.forward_bzip2, self.reverse_bzip2, exists_ok=True)
        self.assertListEqual(fastq.files, [Path(self.forward_bzip2), Path(self.reverse_bzip2)])        
        self.assertEqual(fastq.run, None)
        self.assertEqual(fastq.extension, None)
        self.assertEqual(fastq.sample_name, None)
        self.assertEqual(fastq.orientation, None)


    def test_create_exists_not_ok_raises(self):
        with self.assertRaises(FileExistsError):
            PairedEndFastq(self.forward, self.reverse, exists_ok=False)

    def test_create_missing_directory_raises(self):
        with self.assertRaises(FileNotFoundError):
            PairedEndFastq('/a/foo', '/b/bar')

    def test_same_file_raises(self):
        with self.assertRaises(ValueError):
            PairedEndFastq(self.forward, self.forward, exists_ok=False)
        with self.assertRaises(ValueError):
            PairedEndFastq(self.forward, self.forward, exists_ok=True)

    def test_one_file_raises(self):
        iterator = iter([self.forward])
        with self.assertRaises(ValueError):
            PairedEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}")

    def test_pop_raises_at_end(self):
        iterator = iter([self.forward, self.reverse])
        PairedEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}", exists_ok=True)
        with self.assertRaises(StopIteration):
            PairedEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}")

    def test_pop(self):
        iterator = iter([self.forward, self.reverse])
        PairedEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}", exists_ok=True)

    def test_split(self):
        self.read1_f = dedent("""\
                                  @EU861894-140/1
                                  CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                  +
                                  ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
                                  """).strip().encode()
        self.read2_f = dedent("""\
                                  @EU861894-138/1
                                  AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                  +
                                  @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
                                  """).strip().encode()
        self.read1_r = dedent("""\
                                  @EU861894-140/2
                                  AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                  +
                                  @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                  
                                  """).strip().encode()
        self.read2_r = dedent("""\
                                  @EU861894-138/2
                                  CTTCGGGGGTGGTTAGGCAACCCCCCCCGAAGCGGGGGACAACAGCCTTAAACGGTTCCTAATACCGCATGGTGA
                                  +
                                  BB@FB2@FHB2HFJGFFHJ?8=##JDGHDEIBH?H#HI)EFFEF#C#B#HE?#D?#;#DDCA#:DD>BCB###D'
                                  
                                  """).strip().encode()

        fastq = PairedEndFastq(self.forward, self.reverse, exists_ok=True)
        fastq._SPLIT_BUFFER = 256
        split_fastq = fastq.split(2, self.tempdir.name)
        forward_1, reverse_1 = Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_0.fastq")
        forward_2, reverse_2 = Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_1.fastq")
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        self.assertIn(forward_1, files)
        self.assertIn(reverse_1, files)
        self.assertIn(forward_2, files)
        self.assertIn(reverse_2, files)
        self.assertTrue(file_contents_equals(forward_1, self.read1_f))
        self.assertTrue(file_contents_equals(forward_2, self.read2_f))
        self.assertTrue(file_contents_equals(reverse_1, self.read1_r))
        self.assertTrue(file_contents_equals(reverse_2, self.read2_r))

    def test_join(self):
        self.read2_f = dedent("""\
                                  @EU861894-140/1
                                  CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                  +
                                  ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
                                  """).strip().encode()
        self.read1_f = dedent("""\
                                  @EU861894-138/1
                                  AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                  +
                                  @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
                                  """).strip().encode()
        self.read2_r = dedent("""\
                                  @EU861894-140/2
                                  AAGGAATTGACAGGGGCACGCATAAGCGGTGCGGTATGTGCATTAATTCGTCACTAACTGAAGAACCTCACCAGGCTTTGAAACCCACGGAGAGCGGGAG
                                  +
                                  @1@F1FFEFFA#AGGJB!EHDI434:J?GJI##B#)BJIICJJGEBFIBJ>GGDJIGI#)II<H6=ID#E?CD4##CDEFB#C#CA#-<#?#FCE#!DC'
                                  
                                  """).strip().encode()
        self.read1_r = dedent("""\
                                  @EU861894-138/2
                                  CTTCGGGGGTGGTTAGGCAACCCCCCCCGAAGCGGGGGACAACAGCCTTAAACGGTTCCTAATACCGCATGGTGA
                                  +
                                  BB@FB2@FHB2HFJGFFHJ?8=##JDGHDEIBH?H#HI)EFFEF#C#B#HE?#D?#;#DDCA#:DD>BCB###D'
                                  
                                  """).strip().encode()
        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq", 'wb+') as temp_file_forward_0,\
            open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_0.fastq", 'wb+') as temp_file_reverse_0,\
            open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq", 'wb+') as temp_file_forward_1,\
            open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R2_001_1.fastq", 'wb+') as temp_file_reverse_1:

            temp_file_forward_0.write(self.read1_f)
            temp_file_forward_0.flush()
            temp_file_forward_1.write(self.read2_f)
            temp_file_forward_1.flush()
            temp_file_reverse_0.write(self.read1_r)
            temp_file_reverse_0.flush()
            temp_file_reverse_1.write(self.read2_r)
            temp_file_reverse_1.flush()

        p_fastq_0 = PairedEndFastq(temp_file_forward_0.name, temp_file_reverse_0.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)
        p_fastq_1 = PairedEndFastq(temp_file_forward_1.name, temp_file_reverse_1.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)

        result = p_fastq_1.join(p_fastq_0)
        foward_result, reverse_result = result.files
        self.assertTrue(file_contents_equals(foward_result, fastq_forward()))
        self.assertTrue(file_contents_equals(reverse_result, fastq_reverse()))



class TestSingleEndFastq(unittest.TestCase):
    def setUp(self):
        self.tempdir = TemporaryDirectory()
        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward:
            temp_file_forward.write(fastq_forward())
            temp_file_forward.flush()
            self.forward = self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq"
    
    def tearDown(self):
        self.tempdir.cleanup()

    def test_create_uncompressed(self):
        SingleEndFastq(self.forward, exists_ok=True)

    def test_create_missing_file_raises(self):
        with self.assertRaises(ValueError):
            SingleEndFastq('/tmp/foo', exists_ok=True)

    def test_create_input_is_not_file(self):
        with self.assertRaises(ValueError):
            SingleEndFastq('/tmp/', exists_ok=True)

    def test_pop(self):
        iterator = iter([self.forward])
        SingleEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}", exists_ok=True)

    def test_pop_raises(self):
        iterator = iter([self.forward])
        SingleEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}", exists_ok=True)
        with self.assertRaises(StopIteration):
            SingleEndFastq.pop(iterator, "{run:25}_R{orientation:1}_001{extension}")

    def test_split(self):
        self.read1_f = dedent("""\
                                  @EU861894-140/1
                                  CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                  +
                                  ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
                                  """).strip().encode()
        self.read2_f = dedent("""\
                                  @EU861894-138/1
                                  AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                  +
                                  @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
                                  """).strip().encode()

        fastq = SingleEndFastq(self.forward, exists_ok=True)
        fastq._SPLIT_BUFFER = 256
        split_fastq = fastq.split(2, self.tempdir.name)
        files = list(Path(self.tempdir.name).glob("*.fastq"))
        forward_1, forward_2 = Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq"), Path(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq")
        self.assertIn(forward_1, files)
        self.assertIn(forward_2, files)
        self.assertTrue(file_contents_equals(forward_1, self.read1_f))
        self.assertTrue(file_contents_equals(forward_2, self.read2_f))

    def test_join(self):
        self.read2_f = dedent("""\
                                  @EU861894-140/1
                                  CCGATCTCTCGGCCTGCCCGGGGATCTCAAACNCTGGTAAGCTTCTCCGGTTAGTGACGAATCACTGTACCTGCTCCACCGCTTGTGCGGGCCCTCGTCA
                                  +
                                  ??CFFF?;HHAH#III#I:IHJIJG#JIJJI?3IIJ0JJ#G3JGHG#I90?HIJG9JEIIBD#C#G@#C=)I@#BHBBCDCD;ACDCB;??CD>#D#:DA
                                  
                                  """).strip().encode()
        self.read1_f = dedent("""\
                                  @EU861894-138/1
                                  AGAGCGCATCCACATGTGGTCCCCCGCTTCGGGGCAGGTTGCCCACGTGTTACGCGACCGTTCGCCATTAACCAC
                                  +
                                  @CCF:#@DHHG?BAJJG0BII#GI8;FJIBFBHGJI>+EC=BECBDCHDEAAD#=E6DDFF=9CD#A#8H@@>#D
                                  
                                  """).strip().encode()

        with open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_0.fastq", 'wb+') as temp_file_forward_0,\
             open(self.tempdir.name + "/17146FL-13-01-01_S97_L002_R1_001_1.fastq", 'wb+') as temp_file_forward_1:
            temp_file_forward_0.write(self.read1_f)
            temp_file_forward_0.flush()
            temp_file_forward_1.write(self.read2_f)
            temp_file_forward_1.flush()

        s_fastq_0 = SingleEndFastq(temp_file_forward_0.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)
        s_fastq_1 = SingleEndFastq(temp_file_forward_1.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)

        result = s_fastq_1.join(s_fastq_0)
        [foward_result] = result.files
        self.assertTrue(file_contents_equals(foward_result, fastq_forward()))


class TestSequencingData(unittest.TestCase):
    def setUp(self):
        self.tempdir_paired = TemporaryDirectory()
        with open(self.tempdir_paired.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward, open(self.tempdir_paired.name + "/17146FL-13-01-01_S97_L002_R2_001.fastq", 'wb+') as temp_file_reverse:
            temp_file_forward.write(fastq_forward())
            temp_file_reverse.write(fastq_reverse())
            temp_file_forward.flush()
            temp_file_reverse.flush()
            self.p_fastq = PairedEndFastq(temp_file_forward.name, temp_file_reverse.name, run="17146FL-13-01-01_S97", extension=".fastq", orientation=[1,2], exists_ok=True)
        
        self.tempdir_single = TemporaryDirectory()
        with open(self.tempdir_single.name + "/17146FL-13-01-01_S97_L002_R1_001.fastq", 'wb+') as temp_file_forward:
            temp_file_forward.write(fastq_forward())
            temp_file_forward.flush()
            self.s_fastq = SingleEndFastq(temp_file_forward.name, run="17146FL-13-01-01_S97", extension=".fastq", exists_ok=True)

    def tearDown(self):
        self.tempdir_paired.cleanup()
        self.tempdir_single.cleanup()

    def test_create_single_end_default(self):
        SequencingData([self.s_fastq])
        
    def test_create_single_end_from_directory(self):
        SequencingData.from_directory(SingleEndFastq, self.tempdir_single.name, "{run:25}_R1_001{extension}")

    def test_create_paired_end_default(self):
        SequencingData([self.p_fastq])

    def test_create_paired_end_from_directory(self):
        SequencingData.from_directory(PairedEndFastq, self.tempdir_paired.name, "{run:25}_R{orientation:1}_001{extension}")


class TestFileNameTemplate(unittest.TestCase):
    def test_no_template(self):
        with self.assertRaises(ValueError):
            _FastqFileNameTemplate("")
    
    def test_regex_creation(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        self.assertEquals(fastq_name_template._regex_expression, "(?P<run>.{25})_R(?P<orientation>.{1})_001(?P<extension>.*)")

    def test_regex_creation_escape(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        result = fastq_name_template.parse("17146FL-13-01-01_S97_L002_R1_001.fastq")
        self.assertDictEqual(result, {"extension": ".fastq", "orientation": "1", "run": "17146FL-13-01-01_S97_L002"})

    def test_groups(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        self.assertListEqual(fastq_name_template._groups, ["run", "orientation", "extension"])

    def test_set_template(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        self.assertEquals(fastq_name_template._template, "{run:25}_R{orientation:1}_001{extension}")

    def test_parse_filename(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        result = fastq_name_template.parse("17146FL-13-01-01_S97_L002_R1_001.fastq")
        self.assertDictEqual(result, {"extension": ".fastq", "orientation": "1", "run": "17146FL-13-01-01_S97_L002"})

    def test_parse_filename_special_char(self):
        for special_char in ('$', '^', '.', '|', '?', '!', '*', ']', '(', ')', '[', ']', "\\"):
            template = "{run:25}_" + special_char + "{orientation:1}_001{extension}"
            fastq_name_template = _FastqFileNameTemplate(template)
            to_parse = "17146FL-13-01-01_S97_L002_" + special_char + "1_001.fastq"
            result = fastq_name_template.parse(to_parse)
            self.assertDictEqual(result, {"extension": ".fastq", "orientation": "1", "run": "17146FL-13-01-01_S97_L002"})

        template = "{run:25}_{{{orientation:1}_001{extension}"
        fastq_name_template = _FastqFileNameTemplate(template)
        to_parse = "17146FL-13-01-01_S97_L002_{1_001.fastq"
        result = fastq_name_template.parse(to_parse)
        self.assertDictEqual(result, {"extension": ".fastq", "orientation": "1", "run": "17146FL-13-01-01_S97_L002"})

        template = "{run:25}_}}{orientation:1}_001{extension}"
        fastq_name_template = _FastqFileNameTemplate(template)
        to_parse = "17146FL-13-01-01_S97_L002_}1_001.fastq"
        result = fastq_name_template.parse(to_parse)
        self.assertDictEqual(result, {"extension": ".fastq", "orientation": "1", "run": "17146FL-13-01-01_S97_L002"})


    def test_parse_empty_filename_raises(self):
        fastq_name_template = _FastqFileNameTemplate("{run:25}_R{orientation:1}_001{extension}")
        with self.assertRaises(ValueError):
            fastq_name_template.parse("")

    def test_parse_longest_segment(self):
        fastq_name_template = _FastqFileNameTemplate("{sample_name}_{run:24}_{orientation:1}{extension:9}")
        result = fastq_name_template.parse("LV_1_17146FL-22-01-01_S1_L003_1.fastq.gz")
        self.assertDictEqual(result, {"extension": ".fastq.gz", "orientation": "1", "run": "17146FL-22-01-01_S1_L003", "sample_name": "LV_1"})

if __name__ == '__main__':
    unittest.main()
