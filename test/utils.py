from typing import Union
from pathlib import Path

def file_contents_equals(file_to_check: Union[Path, str], contents: str):
    with file_to_check.open('rb') as fh:
        file_contents = fh.read().strip()
        return file_contents == contents
